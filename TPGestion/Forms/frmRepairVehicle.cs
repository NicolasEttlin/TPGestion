﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using TPGestion.Models;

namespace TPGestion.Forms
{
    public partial class frmRepairVehicle : Form
    {
        int progress = 0;
        int max = 15;

        public frmRepairVehicle()
        {
            InitializeComponent();
        }

        private void frmRepairVehicle_Load(object sender, EventArgs e)
        {
            // Full screen
            this.Bounds = Screen.PrimaryScreen.Bounds;

            MoveRepairButton();
        }

        private void MoveRepairButton()
        {
            // Move and resize randomly the button
            btnRepair.Height = Rand.Next(65, 85);
            btnRepair.Width = Rand.Next(65, 85);
            btnRepair.Top = Rand.Next(50, Height - btnRepair.Height - 50);
            btnRepair.Left = Rand.Next(50, Width - btnRepair.Width - 50);

            ActiveControl = null; // Don't focus the button

            lblProgress.Text = String.Format("{0}/{1}", progress, max);
        }

        /// <summary>
        /// Click on the repair button (increases progress)
        /// </summary>
        private void btnRepair_Click(object sender, EventArgs e)
        {
            progress += 1;

            if (progress >= max)
            {
                MessageBox.Show("Le véhicule est comme neuf !", "Félicitations");
                Close();
            }

            MoveRepairButton();
        }

        /// <summary>
        /// Prevent the form from closing
        /// </summary>
        private void frmRepairVehicle_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (progress < max)
            {
                MessageBox.Show("Vous devez d'abord terminer les réparations", "Erreur", MessageBoxButtons.OK, MessageBoxIcon.Error);
                e.Cancel = true;
            }
        }

        /// <summary>
        /// Change button color on hover
        /// </summary>
        private void btnRepair_MouseHover(object sender, EventArgs e)
        {
            ForeColor = Color.OrangeRed;
        }

        /// <summary>
        /// Cancel hover button change
        /// </summary>
        private void btnRepair_MouseLeave(object sender, EventArgs e)
        {
            ForeColor = Color.Black;
        }
    }
}
