﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using TPGestion.Models;

namespace TPGestion.Forms
{
    public partial class frmGame : Form
    {
        private int _selectedIndex = 0;
        private bool _forceClose = false;

        /// <summary>
        /// The index of the shown event
        /// </summary>
        private int SelectedIndex
        {
            get
            {
                return _selectedIndex;
            }

            set
            {
                _selectedIndex = value;
                LoadCurrentEvent();
            }
        }

        /// <summary>
        /// The event shown
        /// </summary>
        private Event SelectedEvent
        {
            get
            {
                return Game.Events[_selectedIndex];
            }
        }

        private bool IsLastDay
        {
            get
            {
                return Game.Hour == Game.MAX_HOUR;
            }
        }

        private List<RadioButton> actionRadioButtons;

        private RadioButton rdbChecked
        {
            get
            {
                return actionRadioButtons.FirstOrDefault(r => r.Checked);
            }
        }

        public frmGame()
        {
            InitializeComponent();
            actionRadioButtons = new List<RadioButton>() { rdbReplaceDriver, rdbSendMessage, rdbDoNothing };
        }

        /// <summary>
        /// Launch a game day
        /// </summary>
        private void frmGame_Load(object sender, EventArgs e)
        {
            Game.StartNewDay();
            Text = $"Jour {Game.Day}";
            NextHour();
        }

        /// <summary>
        /// Go to the next hour
        /// </summary>
        private void NextHour()
        {
            Game.NextHour();
            lblHour.Text = $"{Game.Hour.ToString().PadLeft(2, '0')}:00";

            if (IsLastDay)
                btnNextHour.Text = "Terminer le jour";

            LoadEvents();
        }

        /// <summary>
        /// Load events
        /// </summary>
        private void LoadEvents()
        {
            List<Event> events = Game.Events;

            if (events.Count == 0)
            {
                lblEvents.Text = "Aucun incident";
                tbxEvent.Text = "Tout va bien !";
                tbxEvent.Select(0, 0);
                btnNextHour.Enabled = true;
                btnNextEvent.Enabled = false;
                btnPreviousEvent.Enabled = false;
                gbxActions.Enabled = false;
            }
            else
            {
                SelectedIndex = 0;
                btnNextHour.Enabled = false;
                gbxActions.Enabled = true;
            }
        }

        /// <summary>
        /// Load the current event
        /// </summary>
        private void LoadCurrentEvent()
        {
            lblEvents.Text = $"Incident {SelectedIndex + 1}/{Game.Events.Count}";
            tbxEvent.Text = SelectedEvent.Description;
            tbxEvent.Select(0, 0);
            btnNextEvent.Enabled = SelectedIndex < (Game.Events.Count - 1);
            btnPreviousEvent.Enabled = SelectedIndex > 0;

            // Driver name
            if (SelectedEvent.Driver == null)
            {
                rdbReplaceDriver.Enabled = false;
            }
            else
            {
                rdbReplaceDriver.Text = $"Remplacer {SelectedEvent.Driver.Name}";
                rdbReplaceDriver.Enabled = true;
            }
        }

        /// <summary>
        /// Close the form
        /// </summary>
        private void frmGame_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (_forceClose)
            {
                return;
            }

            // When the day is ended, return to the home screen
            if (Game.Events.Count == 0 && IsLastDay)
            {
                (new frmHome()).Show();
                return;
            }

            // If the game is ongoing, show a warning message then close the application
            if (MessageBox.Show("Voulez-vous vraiment quitter le jeu ? Vous perderez votre progression.", "Avertissement", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == DialogResult.No)
            {
                e.Cancel = true;
            }
            else
            {
                _forceClose = true;
                Application.Exit();
            }
        }

        /// <summary>
        /// Go to the next/previous event
        /// </summary>
        private void btnsEventsNavigation_Click(object sender, EventArgs e)
        {
            Button btnPressed = sender as Button;

            if (btnPressed.Name == "btnNextEvent")
                SelectedIndex += 1;
            else if (btnPressed.Name == "btnPreviousEvent")
                SelectedIndex -= 1;
        }

        /// <summary>
        /// Go to the next hour/day
        /// </summary>
        private void btnNextHour_Click(object sender, EventArgs e)
        {
            if (!IsLastDay)
                NextHour();
            else
                Close(); // Close the game form and go back to the home screen
        }

        /// <summary>
        /// Enable the RadioButton confirm button only when a RadioButton is selected
        /// </summary>
        private void rdbAction_CheckedChanged(object sender, EventArgs e)
            => btnFixEvent.Enabled = (rdbChecked != null);

        /// <summary>
        /// React to an event
        /// </summary>
        private void btnFixEvent_Click(object sender, EventArgs e)
        {
            string action = rdbChecked.Tag.ToString();

            switch (action)
            {
                case "replace": // Replace the driver
                    frmChooseAvailableDriver frmDriver = new frmChooseAvailableDriver();
                    frmDriver.ShowDialog();

                    if (frmDriver.DialogResult == DialogResult.OK) // A driver was chosen
                    {
                        Line line = SelectedEvent.Driver.getLine();
                        line.Drivers.Add(frmDriver.SelectedDriver); // Affect the new driver
                        line.Drivers.Remove(SelectedEvent.Driver);

                        Game.Events.Remove(SelectedEvent);
                        LoadEvents();
                    }

                    break;

                case "message": // Send a message
                    frmCommunicationChannels frmChannel = new frmCommunicationChannels();
                    frmChannel.ShowDialog();

                    if (frmChannel.TerminalEnabled)
                    {
                        frmCommunication f = new frmCommunication()
                        {
                            Theme = frmCommunication.Type.Terminal,
                            Message = SelectedEvent.MessageTerminal
                        };

                        f.ShowDialog();
                    }

                    if (frmChannel.WebEnabled)
                    {
                        frmCommunication f = new frmCommunication()
                        {
                            Theme = frmCommunication.Type.Code,
                            Message = SelectedEvent.MessageWeb
                        };

                        f.ShowDialog();
                    }

                    Game.Events.Remove(SelectedEvent);
                    LoadEvents();

                    break;
                default: // Do nothing
                    Game.Events.Remove(SelectedEvent);
                    LoadEvents();
                    break;
            }

            rdbChecked.Checked = false;
            rdbReplaceDriver.Text = "Remplacer le conducteur";
        }
    }
}
