﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TPGestion.Models
{
    /// <summary>
    /// Bus line model
    /// </summary>
    public class Line
    {
        private string[] _stops;
        private List<Driver> _drivers = new List<Driver>();

        /// <summary>
        /// The line's name
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// How many drivers must be working on the line
        /// </summary>
        public int DriversSlots { get; set; }

        /// <summary>
        /// Line stops
        /// </summary>
        public string[] Stops => _stops;

        /// <summary>
        /// Drivers affected to the line
        /// </summary>
        public List<Driver> Drivers => _drivers;

        public string From { get { return Stops[0]; } }

        public string To { get { return Stops[Stops.Length - 1]; } }

        /// <summary>
        /// Tab name with affected drivers count (Ligne XX (2/4))
        /// </summary>
        public string TabName
        {
            get
            {
                return $"Ligne {Name} ({Drivers.Count()}/{DriversSlots})";
            }
        }

        /// <summary>
        /// Line route résumé
        /// </summary>
        public string Resume
        {
            get
            {
                return String.Format("{0} : {1} - {2}", Name, From, To);
            }
        }
        
        /// <summary>
        /// Cast a line to string
        /// </summary>
        public override string ToString()
            => Resume;

        private Line()
        {

        }

        public static Line generateRandom()
        {
            string[][] journeys = {
                new string[] { "Jardin Botanique", "Gare Cornavin", "Stand", "Plainpalais", "Hôpital" },
                new string[] { "Onex-Cité", "De-Ternier", "Jonction", "Bel-Air", "Rive", "Genève-Plage" },
                new string[] { "Gardiol", "Petit-Saconnex", "Servette", "Gare Cornavin", "Bel-Air", "Claparède", "Crêts-de-Champel" },
                new string[] { "Aéroport", "Grand-Saconnex-Place", "Nations", "Poste", "Gare Cornavin", "Bel-Air", "Claparède", "Rieu", "Thônex-Vallard" },
                new string[] { "Vernier-Village", "Renfile", "Gare Cornavin", "Mont-Blanc", "Rive", "Genève-Plage" },
                new string[] { "Hôpital", "Rive", "Bel-Air", "Mercier", "Aïre", "Tours Lignon" },
                new string[] { "OMS", "Appia", "Nations", "Grottes", "Gare Cornavin", "Mont-Blanc", "Rive", "Florissant", "Veyrier" },
                new string[] { "Aéroport", "Guye", "Gare Cornavin", "Bel-Air", "Rive" },
                new string[] { "Jardin Botanique", "Délices", "Jonction", "Épinettes", "Carouge-Marché", "Bout-du-Monde" },
                new string[] { "Palettes", "Bachet-de-Pesay", "Carouge", "Augustins", "Plainpalais", "Bel-Air", "Rive", "Moillesulaz" },
                new string[] { "P+R Bernex", "Onex-Salle communale", "Jonction", "Bel-Air", "Gare Cornavin", "Servette", "Blandonnet", "Meyrin-Gravière" },
                new string[] { "Nations", "Gare Cornavin", "Stand", "Plainpalais", "Acacias", "P+R Étoile", "Palettes" },
                new string[] { "Carouge-Rondeau", "Augustins", "Plainpalais", "Bel-Air", "Gare Cornavin", "Servette", "Blandonnet", "CERN" }
            };

            return new Line()
            {
                Name = Rand.Next(1, 20).ToString(),
                _stops = journeys[Rand.Next(journeys.Length)],
                DriversSlots = 1
            };
        }

        public string getPresentationText()
        {
            StringBuilder p = new StringBuilder();

            p.AppendLine("PROJET DE LIGNE " + From.ToUpper() + "-" + To.ToUpper());
            p.AppendLine();

            p.AppendLine("==========================");
            p.AppendLine("PARCOURS");
            p.AppendLine("==========================");

            // Route
            p.Append(String.Format("La ligne comporte {0} arrêts. ", Stops.Length));

            string[] startPhrases = { "Elle commence à", "Son point de départ se situe à", "Elle débute à", "Elle part de" };
            string[] stopsPhases = { "Puis, elle s'arrête à", "Ensuite, elle continue via", "Après, elle va vers", "Juste après, elle stoppe à l'arrêt" };
            string[] endPhrases = { "Elle termine sa course", "Son terminus se situe", "La fin de la ligne est" };

            p.Append(String.Format("{0} {1}. ", Rand.GetRandomElement(startPhrases), From));
            foreach (string stop in Stops.Skip(1).Take(Stops.Length - 2)) // Stops without first and last stops
            {
                p.Append(String.Format("{0} {1}. ", Rand.GetRandomElement(stopsPhases), stop));
            }
            p.Append(String.Format("{0} à {1}. ", Rand.GetRandomElement(endPhrases), To));

            p.AppendLine(Environment.NewLine);

            p.AppendLine("==========================");
            p.AppendLine("EXPLOITATION");
            p.AppendLine("==========================");
            p.AppendLine("Cette ligne sera exploitée par notre compagnie à partir du " + DateTime.Now.ToString("dd.MM.yyy") + " de manière permanente. Elle sera exploitée par autobus.");
            p.AppendLine();

            p.AppendLine("==========================");
            p.AppendLine("REVENUS");
            p.AppendLine("==========================");
            p.AppendLine("Nous nous attendons à ce qu'environ 50 personnes prennent la ligne chaque jour. Comme toutes les autres lignes de notre réseau, le prix du billet est de 3.- CHF par voyage.");
            p.AppendLine();


            return p.ToString();
        }
    }
}
