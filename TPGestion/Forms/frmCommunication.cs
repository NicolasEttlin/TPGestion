﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace TPGestion.Forms
{
    public partial class frmCommunication : Form
    {
        public enum Type { Terminal, Code };

        public Type Theme { get; set; }

        public string Message { get; set; }

        private string _placeholder;

        /// <summary>
        /// Is the message entierly typed
        /// </summary>
        public bool MessageComplete
        {
            get
            {
                return tbxMessage.TextLength >= Message.Length;
            }
        }

        /// <summary>
        /// Create a frmCommunication with an appearance theme
        /// </summary>
        /// <param name="type">Theme to use</param>
        public frmCommunication()
        {
            InitializeComponent();
        }

        /// <summary>
        /// Load the form and apply the theme
        /// </summary>
        private void frmCommunication_Load(object sender, EventArgs e)
        {
            switch (Theme)
            {
                case Type.Terminal:
                    tbxMessage.BackColor = Color.Black;
                    tbxMessage.Font = new Font("Consolas", 24F, FontStyle.Bold, GraphicsUnit.Point, 0);
                    tbxMessage.ForeColor = Color.Orange;
                    Icon = Properties.Resources.Info;
                    _placeholder = "VEUILLEZ ENTRER UN MESSAGE";
                    Text = "Borne d'information";
                    break;
                case Type.Code:
                    tbxMessage.Font = new Font("Consolas", 14);
                    tbxMessage.BackColor = Color.FromArgb(255, 38, 50, 56);
                    tbxMessage.ForeColor = Color.White;
                    Icon = Properties.Resources.Atom;
                    _placeholder = "<!-- Veuillez entrer un message -->";
                    Text = "Atom";
                    break;
                default:
                    break;
            }

            tbxMessage.Text = _placeholder;
        }


        /// <summary>
        /// Key press
        /// </summary>
        private void tbxMessage_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (MessageComplete) // End
            {
                e.KeyChar = ' ';
                Close();
                return;
            }

            if (tbxMessage.Text == _placeholder)
                tbxMessage.Clear();

            e.KeyChar = Message[tbxMessage.TextLength];
        }

        private void frmCommunication_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (!MessageComplete)
            {
                MessageBox.Show("Vous devez taper le message d'information en entier.", "Erreur", MessageBoxButtons.OK, MessageBoxIcon.Error);
                e.Cancel = true;
            }
        }
    }
}
