﻿namespace TPGestion.Forms
{
    partial class frmManageDrivers
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.lbxDrivers = new System.Windows.Forms.ListBox();
            this.btnNewDriver = new System.Windows.Forms.Button();
            this.gbxDriver = new System.Windows.Forms.GroupBox();
            this.tableLayoutPanel2 = new System.Windows.Forms.TableLayoutPanel();
            this.btnFireDriver = new System.Windows.Forms.Button();
            this.lblDriverName = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.lblAffectation = new System.Windows.Forms.Label();
            this.btnAffect = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            this.tableLayoutPanel1.SuspendLayout();
            this.gbxDriver.SuspendLayout();
            this.tableLayoutPanel2.SuspendLayout();
            this.SuspendLayout();
            // 
            // splitContainer1
            // 
            this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer1.Location = new System.Drawing.Point(0, 0);
            this.splitContainer1.Name = "splitContainer1";
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add(this.tableLayoutPanel1);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.Controls.Add(this.gbxDriver);
            this.splitContainer1.Size = new System.Drawing.Size(784, 461);
            this.splitContainer1.SplitterDistance = 261;
            this.splitContainer1.TabIndex = 0;
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 1;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.Controls.Add(this.lbxDrivers, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.btnNewDriver, 0, 1);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 2;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(261, 461);
            this.tableLayoutPanel1.TabIndex = 0;
            // 
            // lbxDrivers
            // 
            this.lbxDrivers.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lbxDrivers.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbxDrivers.FormattingEnabled = true;
            this.lbxDrivers.IntegralHeight = false;
            this.lbxDrivers.ItemHeight = 20;
            this.lbxDrivers.Location = new System.Drawing.Point(3, 3);
            this.lbxDrivers.Name = "lbxDrivers";
            this.lbxDrivers.Size = new System.Drawing.Size(255, 405);
            this.lbxDrivers.TabIndex = 0;
            this.lbxDrivers.SelectedIndexChanged += new System.EventHandler(this.lbxDrivers_SelectedIndexChanged);
            // 
            // btnNewDriver
            // 
            this.btnNewDriver.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnNewDriver.Location = new System.Drawing.Point(3, 414);
            this.btnNewDriver.Name = "btnNewDriver";
            this.btnNewDriver.Size = new System.Drawing.Size(255, 44);
            this.btnNewDriver.TabIndex = 1;
            this.btnNewDriver.Text = "&Embaucher";
            this.btnNewDriver.UseVisualStyleBackColor = true;
            this.btnNewDriver.Click += new System.EventHandler(this.btnNewDriver_Click);
            // 
            // gbxDriver
            // 
            this.gbxDriver.Controls.Add(this.tableLayoutPanel2);
            this.gbxDriver.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gbxDriver.Enabled = false;
            this.gbxDriver.Location = new System.Drawing.Point(0, 0);
            this.gbxDriver.Name = "gbxDriver";
            this.gbxDriver.Size = new System.Drawing.Size(519, 461);
            this.gbxDriver.TabIndex = 0;
            this.gbxDriver.TabStop = false;
            this.gbxDriver.Text = "Aucun conducteur sélectionné";
            // 
            // tableLayoutPanel2
            // 
            this.tableLayoutPanel2.ColumnCount = 1;
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel2.Controls.Add(this.btnFireDriver, 0, 4);
            this.tableLayoutPanel2.Controls.Add(this.lblDriverName, 0, 0);
            this.tableLayoutPanel2.Controls.Add(this.label1, 0, 1);
            this.tableLayoutPanel2.Controls.Add(this.lblAffectation, 0, 2);
            this.tableLayoutPanel2.Controls.Add(this.btnAffect, 0, 3);
            this.tableLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel2.Location = new System.Drawing.Point(3, 16);
            this.tableLayoutPanel2.Name = "tableLayoutPanel2";
            this.tableLayoutPanel2.RowCount = 6;
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20.18585F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 13.1208F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 14.82074F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 51.8726F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.tableLayoutPanel2.Size = new System.Drawing.Size(513, 442);
            this.tableLayoutPanel2.TabIndex = 0;
            // 
            // btnFireDriver
            // 
            this.btnFireDriver.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnFireDriver.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnFireDriver.ForeColor = System.Drawing.Color.Crimson;
            this.btnFireDriver.Location = new System.Drawing.Point(3, 343);
            this.btnFireDriver.Name = "btnFireDriver";
            this.btnFireDriver.Padding = new System.Windows.Forms.Padding(5);
            this.btnFireDriver.Size = new System.Drawing.Size(507, 44);
            this.btnFireDriver.TabIndex = 4;
            this.btnFireDriver.Text = "&Licencier";
            this.btnFireDriver.UseVisualStyleBackColor = true;
            this.btnFireDriver.Click += new System.EventHandler(this.btnFireDriver_Click);
            // 
            // lblDriverName
            // 
            this.lblDriverName.AutoSize = true;
            this.lblDriverName.Dock = System.Windows.Forms.DockStyle.Top;
            this.lblDriverName.Font = new System.Drawing.Font("Segoe UI Black", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDriverName.Location = new System.Drawing.Point(3, 0);
            this.lblDriverName.Name = "lblDriverName";
            this.lblDriverName.Padding = new System.Windows.Forms.Padding(0, 0, 0, 20);
            this.lblDriverName.Size = new System.Drawing.Size(507, 65);
            this.lblDriverName.TabIndex = 0;
            this.lblDriverName.Text = "Nom";
            // 
            // label1
            // 
            this.label1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.label1.Font = new System.Drawing.Font("Segoe UI", 14.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(3, 82);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(507, 31);
            this.label1.TabIndex = 1;
            this.label1.Text = "Affectation";
            this.label1.TextAlign = System.Drawing.ContentAlignment.BottomLeft;
            // 
            // lblAffectation
            // 
            this.lblAffectation.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblAffectation.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblAffectation.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblAffectation.Location = new System.Drawing.Point(3, 123);
            this.lblAffectation.Margin = new System.Windows.Forms.Padding(3, 10, 3, 10);
            this.lblAffectation.Name = "lblAffectation";
            this.lblAffectation.Padding = new System.Windows.Forms.Padding(5);
            this.lblAffectation.Size = new System.Drawing.Size(507, 30);
            this.lblAffectation.TabIndex = 2;
            this.lblAffectation.Text = "Affectation";
            this.lblAffectation.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // btnAffect
            // 
            this.btnAffect.Location = new System.Drawing.Point(3, 166);
            this.btnAffect.Name = "btnAffect";
            this.btnAffect.Size = new System.Drawing.Size(138, 33);
            this.btnAffect.TabIndex = 3;
            this.btnAffect.Text = "Modifier l\'affectation";
            this.btnAffect.UseVisualStyleBackColor = true;
            this.btnAffect.Click += new System.EventHandler(this.btnAffect_Click);
            // 
            // frmManageDrivers
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(784, 461);
            this.Controls.Add(this.splitContainer1);
            this.MinimumSize = new System.Drawing.Size(800, 500);
            this.Name = "frmManageDrivers";
            this.ShowIcon = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Gestion des conducteurs";
            this.Load += new System.EventHandler(this.frmManageDrivers_Load);
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
            this.splitContainer1.ResumeLayout(false);
            this.tableLayoutPanel1.ResumeLayout(false);
            this.gbxDriver.ResumeLayout(false);
            this.tableLayoutPanel2.ResumeLayout(false);
            this.tableLayoutPanel2.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.SplitContainer splitContainer1;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.ListBox lbxDrivers;
        private System.Windows.Forms.Button btnNewDriver;
        private System.Windows.Forms.GroupBox gbxDriver;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel2;
        private System.Windows.Forms.Label lblDriverName;
        private System.Windows.Forms.Button btnFireDriver;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label lblAffectation;
        private System.Windows.Forms.Button btnAffect;
    }
}