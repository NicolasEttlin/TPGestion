﻿namespace TPGestion.Forms
{
    partial class frmAffectDrivers
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.gbxAvailableDrivers = new System.Windows.Forms.GroupBox();
            this.lsbAvailableDrivers = new System.Windows.Forms.ListBox();
            this.tableLayoutPanel2 = new System.Windows.Forms.TableLayoutPanel();
            this.btnAllLeft = new System.Windows.Forms.Button();
            this.btnLeft = new System.Windows.Forms.Button();
            this.btnRight = new System.Windows.Forms.Button();
            this.btnAllRight = new System.Windows.Forms.Button();
            this.tcLines = new System.Windows.Forms.TabControl();
            this.tableLayoutPanel1.SuspendLayout();
            this.gbxAvailableDrivers.SuspendLayout();
            this.tableLayoutPanel2.SuspendLayout();
            this.SuspendLayout();
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 3;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 75F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.Controls.Add(this.gbxAvailableDrivers, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.tableLayoutPanel2, 1, 0);
            this.tableLayoutPanel1.Controls.Add(this.tcLines, 2, 0);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 1;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(711, 393);
            this.tableLayoutPanel1.TabIndex = 0;
            // 
            // gbxAvailableDrivers
            // 
            this.gbxAvailableDrivers.Controls.Add(this.lsbAvailableDrivers);
            this.gbxAvailableDrivers.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gbxAvailableDrivers.Location = new System.Drawing.Point(10, 15);
            this.gbxAvailableDrivers.Margin = new System.Windows.Forms.Padding(10, 15, 10, 10);
            this.gbxAvailableDrivers.Name = "gbxAvailableDrivers";
            this.gbxAvailableDrivers.Size = new System.Drawing.Size(298, 368);
            this.gbxAvailableDrivers.TabIndex = 0;
            this.gbxAvailableDrivers.TabStop = false;
            this.gbxAvailableDrivers.Text = "Conducteurs disponibles";
            // 
            // lsbAvailableDrivers
            // 
            this.lsbAvailableDrivers.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lsbAvailableDrivers.Font = new System.Drawing.Font("Arial", 14F);
            this.lsbAvailableDrivers.FormattingEnabled = true;
            this.lsbAvailableDrivers.IntegralHeight = false;
            this.lsbAvailableDrivers.ItemHeight = 22;
            this.lsbAvailableDrivers.Location = new System.Drawing.Point(3, 16);
            this.lsbAvailableDrivers.Name = "lsbAvailableDrivers";
            this.lsbAvailableDrivers.Size = new System.Drawing.Size(292, 349);
            this.lsbAvailableDrivers.TabIndex = 0;
            this.lsbAvailableDrivers.SelectedIndexChanged += new System.EventHandler(this.lbx_SelectedIndexChanged);
            // 
            // tableLayoutPanel2
            // 
            this.tableLayoutPanel2.ColumnCount = 1;
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel2.Controls.Add(this.btnAllLeft, 0, 4);
            this.tableLayoutPanel2.Controls.Add(this.btnLeft, 0, 3);
            this.tableLayoutPanel2.Controls.Add(this.btnRight, 0, 2);
            this.tableLayoutPanel2.Controls.Add(this.btnAllRight, 0, 1);
            this.tableLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel2.Location = new System.Drawing.Point(321, 3);
            this.tableLayoutPanel2.Name = "tableLayoutPanel2";
            this.tableLayoutPanel2.RowCount = 6;
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 40F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 40F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 40F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 40F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel2.Size = new System.Drawing.Size(69, 387);
            this.tableLayoutPanel2.TabIndex = 1;
            // 
            // btnAllLeft
            // 
            this.btnAllLeft.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnAllLeft.Enabled = false;
            this.btnAllLeft.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.btnAllLeft.Location = new System.Drawing.Point(3, 236);
            this.btnAllLeft.Name = "btnAllLeft";
            this.btnAllLeft.Size = new System.Drawing.Size(63, 34);
            this.btnAllLeft.TabIndex = 4;
            this.btnAllLeft.Text = "<<";
            this.btnAllLeft.UseVisualStyleBackColor = true;
            this.btnAllLeft.Click += new System.EventHandler(this.btnAllLeft_Click);
            // 
            // btnLeft
            // 
            this.btnLeft.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnLeft.Enabled = false;
            this.btnLeft.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.btnLeft.Location = new System.Drawing.Point(3, 196);
            this.btnLeft.Name = "btnLeft";
            this.btnLeft.Size = new System.Drawing.Size(63, 34);
            this.btnLeft.TabIndex = 3;
            this.btnLeft.Text = "<";
            this.btnLeft.UseVisualStyleBackColor = true;
            this.btnLeft.Click += new System.EventHandler(this.btnLeft_Click);
            // 
            // btnRight
            // 
            this.btnRight.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnRight.Enabled = false;
            this.btnRight.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.btnRight.Location = new System.Drawing.Point(3, 156);
            this.btnRight.Name = "btnRight";
            this.btnRight.Size = new System.Drawing.Size(63, 34);
            this.btnRight.TabIndex = 2;
            this.btnRight.Text = ">";
            this.btnRight.UseVisualStyleBackColor = true;
            this.btnRight.Click += new System.EventHandler(this.btnRight_Click);
            // 
            // btnAllRight
            // 
            this.btnAllRight.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnAllRight.Enabled = false;
            this.btnAllRight.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.btnAllRight.Location = new System.Drawing.Point(3, 116);
            this.btnAllRight.Name = "btnAllRight";
            this.btnAllRight.Size = new System.Drawing.Size(63, 34);
            this.btnAllRight.TabIndex = 0;
            this.btnAllRight.Text = ">>";
            this.btnAllRight.UseVisualStyleBackColor = true;
            this.btnAllRight.Click += new System.EventHandler(this.btnAllRight_Click);
            // 
            // tcLines
            // 
            this.tcLines.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tcLines.Location = new System.Drawing.Point(396, 9);
            this.tcLines.Margin = new System.Windows.Forms.Padding(3, 9, 3, 9);
            this.tcLines.Name = "tcLines";
            this.tcLines.SelectedIndex = 0;
            this.tcLines.Size = new System.Drawing.Size(312, 375);
            this.tcLines.TabIndex = 2;
            this.tcLines.SelectedIndexChanged += new System.EventHandler(this.tcLines_SelectedIndexChanged);
            // 
            // frmAffectDrivers
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(711, 393);
            this.Controls.Add(this.tableLayoutPanel1);
            this.MinimumSize = new System.Drawing.Size(500, 300);
            this.Name = "frmAffectDrivers";
            this.ShowIcon = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Affecter des conducteurs aux lignes";
            this.Load += new System.EventHandler(this.frmAffectationConducteur_Load);
            this.tableLayoutPanel1.ResumeLayout(false);
            this.gbxAvailableDrivers.ResumeLayout(false);
            this.tableLayoutPanel2.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.GroupBox gbxAvailableDrivers;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel2;
        private System.Windows.Forms.Button btnAllRight;
        private System.Windows.Forms.Button btnAllLeft;
        private System.Windows.Forms.Button btnLeft;
        private System.Windows.Forms.Button btnRight;
        private System.Windows.Forms.TabControl tcLines;
        private System.Windows.Forms.ListBox lsbAvailableDrivers;
    }
}