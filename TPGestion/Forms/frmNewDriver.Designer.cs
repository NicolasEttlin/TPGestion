﻿namespace TPGestion.Forms
{
    partial class frmNewDriver
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tbxMotivationText = new System.Windows.Forms.TextBox();
            this.lblName = new System.Windows.Forms.Label();
            this.btnPass = new System.Windows.Forms.Button();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.btnBuy = new System.Windows.Forms.Button();
            this.tableLayoutPanel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // tbxMotivationText
            // 
            this.tbxMotivationText.Font = new System.Drawing.Font("Segoe Print", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbxMotivationText.Location = new System.Drawing.Point(243, 9);
            this.tbxMotivationText.Multiline = true;
            this.tbxMotivationText.Name = "tbxMotivationText";
            this.tbxMotivationText.ReadOnly = true;
            this.tbxMotivationText.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.tbxMotivationText.Size = new System.Drawing.Size(458, 347);
            this.tbxMotivationText.TabIndex = 0;
            this.tbxMotivationText.TabStop = false;
            this.tbxMotivationText.Text = "Lettre de motivation";
            // 
            // lblName
            // 
            this.lblName.AutoEllipsis = true;
            this.lblName.Font = new System.Drawing.Font("Arial", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblName.Location = new System.Drawing.Point(12, 9);
            this.lblName.Name = "lblName";
            this.lblName.Size = new System.Drawing.Size(225, 26);
            this.lblName.TabIndex = 1;
            this.lblName.Text = "Nom du postulant";
            // 
            // btnPass
            // 
            this.btnPass.BackColor = System.Drawing.Color.Transparent;
            this.btnPass.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnPass.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnPass.ForeColor = System.Drawing.Color.Crimson;
            this.btnPass.Location = new System.Drawing.Point(3, 3);
            this.btnPass.Name = "btnPass";
            this.btnPass.Size = new System.Drawing.Size(158, 42);
            this.btnPass.TabIndex = 2;
            this.btnPass.Text = "&Passer";
            this.btnPass.UseVisualStyleBackColor = false;
            this.btnPass.Click += new System.EventHandler(this.btnPass_Click);
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 2;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 35.89743F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 64.10256F));
            this.tableLayoutPanel1.Controls.Add(this.btnBuy, 1, 0);
            this.tableLayoutPanel1.Controls.Add(this.btnPass, 0, 0);
            this.tableLayoutPanel1.Location = new System.Drawing.Point(243, 362);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 1;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(458, 48);
            this.tableLayoutPanel1.TabIndex = 3;
            // 
            // btnBuy
            // 
            this.btnBuy.BackColor = System.Drawing.Color.Transparent;
            this.btnBuy.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnBuy.Font = new System.Drawing.Font("Arial", 15.75F, System.Drawing.FontStyle.Bold);
            this.btnBuy.ForeColor = System.Drawing.Color.Green;
            this.btnBuy.Location = new System.Drawing.Point(167, 3);
            this.btnBuy.Name = "btnBuy";
            this.btnBuy.Size = new System.Drawing.Size(288, 42);
            this.btnBuy.TabIndex = 3;
            this.btnBuy.Text = "&Embaucher";
            this.btnBuy.UseVisualStyleBackColor = false;
            this.btnBuy.Click += new System.EventHandler(this.btnBuy_Click);
            // 
            // frmNewDriver
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(713, 413);
            this.Controls.Add(this.tableLayoutPanel1);
            this.Controls.Add(this.lblName);
            this.Controls.Add(this.tbxMotivationText);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D;
            this.MaximizeBox = false;
            this.Name = "frmNewDriver";
            this.ShowIcon = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "{Nom}";
            this.Load += new System.EventHandler(this.frmNewDriver_Load);
            this.tableLayoutPanel1.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox tbxMotivationText;
        private System.Windows.Forms.Label lblName;
        private System.Windows.Forms.Button btnPass;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.Button btnBuy;
    }
}