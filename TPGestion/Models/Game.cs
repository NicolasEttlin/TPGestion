﻿using System;
using System.Collections.Generic;

namespace TPGestion.Models
{
    /// <summary>
    /// Main game model
    /// </summary>
    static class Game
    {
        public const int MIN_HOUR = 5;
        public const int MAX_HOUR = 23;

        private static int _money;

        public static event EventHandler OnMoneyChange;

        public static int Money
        {
            get => _money;
            set
            {
                _money = value;
                OnMoneyChange?.Invoke(null, EventArgs.Empty);
            }
        }

        /// <summary>
        /// Current day number
        /// </summary>
        public static int Day { get; private set; }

        public static int Hour { get; private set; }

        /// <summary>
        /// The player's network drivers
        /// </summary>
        public static List<Driver> Drivers { get; private set; }

        /// <summary>
        /// The player's network lines
        /// </summary>
        public static List<Line> Lines { get; private set; }

        public static List<Event> Events { get; private set; }

        /// <summary>
        /// Increment money
        /// </summary>
        public static void GainMoney()
        {
            Money += 1;
        }

        /// <summary>
        /// Intitialize a new game
        /// </summary>
        public static void Initialize()
        {
            Day = 0;

            Events = new List<Event>() { };

            Drivers = new List<Driver>() { };
            Drivers.Add(Driver.generateRandom());

            Lines = new List<Line>() { };
            Lines.Add(Line.generateRandom());
        }

        /// <summary>
        /// Start a new game day
        /// </summary>
        public static void StartNewDay()
        {
            Day += 1;
            Hour = (MIN_HOUR - 1);
        }

        /// <summary>
        /// Go to the next hour
        /// </summary>
        public static void NextHour()
        {
            Hour++;

            // Generate events
            Events.Clear();

            if (Rand.Next(20) == 1) // Non-driver events
                Events.Add(Event.GenerateRandomEvent(null));

            foreach (Driver d in Driver.GetDriversInService()) // Driver events
                if (Rand.Next(40) == 1)
                    Events.Add(Event.GenerateRandomEvent(d));
        }
    }
}