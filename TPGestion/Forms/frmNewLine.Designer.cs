﻿namespace TPGestion.Forms
{
    partial class frmNewLine
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.tlpPresentation = new System.Windows.Forms.TableLayoutPanel();
            this.tbxDescription = new System.Windows.Forms.TextBox();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.gbxIndex = new System.Windows.Forms.GroupBox();
            this.lbxNavigation = new System.Windows.Forms.ListBox();
            this.btnAdd = new System.Windows.Forms.Button();
            this.tmrShow = new System.Windows.Forms.Timer(this.components);
            this.tlpPresentation.SuspendLayout();
            this.tableLayoutPanel1.SuspendLayout();
            this.gbxIndex.SuspendLayout();
            this.SuspendLayout();
            // 
            // tlpPresentation
            // 
            this.tlpPresentation.ColumnCount = 2;
            this.tlpPresentation.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 63.38028F));
            this.tlpPresentation.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 36.61972F));
            this.tlpPresentation.Controls.Add(this.tbxDescription, 0, 0);
            this.tlpPresentation.Controls.Add(this.tableLayoutPanel1, 1, 0);
            this.tlpPresentation.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tlpPresentation.Enabled = false;
            this.tlpPresentation.Location = new System.Drawing.Point(0, 0);
            this.tlpPresentation.Name = "tlpPresentation";
            this.tlpPresentation.RowCount = 1;
            this.tlpPresentation.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tlpPresentation.Size = new System.Drawing.Size(705, 411);
            this.tlpPresentation.TabIndex = 0;
            // 
            // tbxDescription
            // 
            this.tbxDescription.BackColor = System.Drawing.Color.White;
            this.tbxDescription.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tbxDescription.Font = new System.Drawing.Font("Arial Narrow", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbxDescription.Location = new System.Drawing.Point(3, 3);
            this.tbxDescription.Multiline = true;
            this.tbxDescription.Name = "tbxDescription";
            this.tbxDescription.ReadOnly = true;
            this.tbxDescription.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.tbxDescription.Size = new System.Drawing.Size(440, 405);
            this.tbxDescription.TabIndex = 1;
            this.tbxDescription.Text = "Réflexion en cours...";
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 1;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.Controls.Add(this.gbxIndex, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.btnAdd, 0, 1);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(449, 3);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 2;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(253, 405);
            this.tableLayoutPanel1.TabIndex = 2;
            // 
            // gbxIndex
            // 
            this.gbxIndex.Controls.Add(this.lbxNavigation);
            this.gbxIndex.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gbxIndex.Location = new System.Drawing.Point(15, 15);
            this.gbxIndex.Margin = new System.Windows.Forms.Padding(15, 15, 15, 5);
            this.gbxIndex.Name = "gbxIndex";
            this.gbxIndex.Padding = new System.Windows.Forms.Padding(10);
            this.gbxIndex.Size = new System.Drawing.Size(223, 335);
            this.gbxIndex.TabIndex = 1;
            this.gbxIndex.TabStop = false;
            this.gbxIndex.Text = "Table des matières";
            // 
            // lbxNavigation
            // 
            this.lbxNavigation.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lbxNavigation.Font = new System.Drawing.Font("Arial", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbxNavigation.FormattingEnabled = true;
            this.lbxNavigation.ItemHeight = 22;
            this.lbxNavigation.Items.AddRange(new object[] {
            "Parcours",
            "Exploitation",
            "Revenus"});
            this.lbxNavigation.Location = new System.Drawing.Point(10, 23);
            this.lbxNavigation.Name = "lbxNavigation";
            this.lbxNavigation.Size = new System.Drawing.Size(203, 302);
            this.lbxNavigation.TabIndex = 0;
            this.lbxNavigation.SelectedIndexChanged += new System.EventHandler(this.lbxNavigation_SelectedIndexChanged);
            // 
            // btnAdd
            // 
            this.btnAdd.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnAdd.Location = new System.Drawing.Point(13, 355);
            this.btnAdd.Margin = new System.Windows.Forms.Padding(13, 0, 13, 13);
            this.btnAdd.Name = "btnAdd";
            this.btnAdd.Size = new System.Drawing.Size(227, 37);
            this.btnAdd.TabIndex = 2;
            this.btnAdd.Text = "Ajouter cette ligne au réseau";
            this.btnAdd.UseVisualStyleBackColor = true;
            this.btnAdd.Click += new System.EventHandler(this.btnAdd_Click);
            // 
            // tmrShow
            // 
            this.tmrShow.Enabled = true;
            this.tmrShow.Interval = 2000;
            this.tmrShow.Tick += new System.EventHandler(this.tmrShow_Tick);
            // 
            // frmNewLine
            // 
            this.AcceptButton = this.btnAdd;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(705, 411);
            this.Controls.Add(this.tlpPresentation);
            this.MinimumSize = new System.Drawing.Size(585, 200);
            this.Name = "frmNewLine";
            this.ShowIcon = false;
            this.Text = "Nouvelle ligne";
            this.tlpPresentation.ResumeLayout(false);
            this.tlpPresentation.PerformLayout();
            this.tableLayoutPanel1.ResumeLayout(false);
            this.gbxIndex.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel tlpPresentation;
        private System.Windows.Forms.TextBox tbxDescription;
        private System.Windows.Forms.Timer tmrShow;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.GroupBox gbxIndex;
        private System.Windows.Forms.ListBox lbxNavigation;
        private System.Windows.Forms.Button btnAdd;
    }
}