﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using TPGestion.Models;

namespace TPGestion.Forms
{
    /// <summary>
    /// Affect drivers to lines
    /// </summary>
    public partial class frmAffectDrivers : Form
    {
        private Line _lineToLoad;
        private Driver _driverToLoad;

        private List<ListBox> linesListBoxes = new List<ListBox>();

        private Line selectedLine
        {
            get
            {
                return Game.Lines[tcLines.SelectedIndex];
            }
        }

        private ListBox selectedLineListbox
        {
            get
            {
                return linesListBoxes[tcLines.SelectedIndex];
            }
        }

        public frmAffectDrivers()
        {
            InitializeComponent();
        }

        /// <summary>
        /// Create an affectation form with a line open
        /// </summary>
        /// <param name="l"></param>
        public frmAffectDrivers(Line l)
        {
            _lineToLoad = l;
            InitializeComponent();
        }

        /// <summary>
        /// Create an affectation form with a driver selected
        /// </summary>
        /// <param name="l"></param>
        public frmAffectDrivers(Driver d)
        {
            _driverToLoad = d;
            InitializeComponent();
        }

        private void frmAffectationConducteur_Load(object sender, EventArgs e)
        {
            // Available drivers
            foreach (Driver d in Driver.GetAvailableDrivers())
            {
                lsbAvailableDrivers.Items.Add(d.Name);
            }

            // Game lines
            foreach (Line l in Game.Lines)
            {
                TabPage tp = new TabPage(l.TabName);

                // List box
                ListBox lsbLine = new ListBox();
                lsbLine.Dock = DockStyle.Fill;
                lsbLine.Font = new System.Drawing.Font("Arial", 14);
                lsbLine.IntegralHeight = false;
                lsbLine.ItemHeight = 22;
                lsbLine.SelectedIndexChanged += lbx_SelectedIndexChanged;

                // List box items
                foreach (Driver d in l.Drivers)
                {
                    lsbLine.Items.Add(d.Name);
                }

                linesListBoxes.Add(lsbLine);
                tp.Controls.Add(lsbLine);

                tcLines.TabPages.Add(tp);

                if (l == _lineToLoad)
                {
                    tcLines.SelectedTab = tp;
                }

                if (l.Drivers.Contains(_driverToLoad))
                {
                    tcLines.SelectedTab = tp;
                    lsbLine.SelectedItem = _driverToLoad.Name;
                }
            }

            UpdateState();

            // Select the driver from the available drivers list
            if (_driverToLoad != null)
                lsbAvailableDrivers.SelectedItem = _driverToLoad.Name;
        }

        private void lbx_SelectedIndexChanged(object sender, EventArgs e)
        {
            UpdateState();
        }

        /// <summary>
        /// Update the buttons states and the current tab name
        /// </summary>
        private void UpdateState()
        {
            // Buttons states
            btnAllRight.Enabled = lsbAvailableDrivers.Items.Count > 0;
            btnRight.Enabled = lsbAvailableDrivers.SelectedItems.Count > 0;

            if (tcLines.SelectedIndex != -1)
            {
                btnLeft.Enabled = selectedLineListbox.SelectedItems.Count > 0;
                btnAllLeft.Enabled = selectedLineListbox.Items.Count > 0;
            }

            // Current tab name
            tcLines.SelectedTab.Text = selectedLine.TabName;
        }

        /// <summary>
        /// Move a driver to the right (from available to a line)
        /// </summary>
        private void btnRight_Click(object sender, EventArgs e)
        {
            int driverIndex = lsbAvailableDrivers.SelectedIndex;

            // Update the model
            Driver d = Driver.findDriver(lsbAvailableDrivers.SelectedItem.ToString());
            selectedLine.Drivers.Add(d);

            // Update listboxes
            lsbAvailableDrivers.Items.RemoveAt(driverIndex);
            selectedLineListbox.Items.Add(d.Name);

            UpdateState();
        }

        /// <summary>
        /// Move a driver to the left (from a line to available)
        /// </summary>
        private void btnLeft_Click(object sender, EventArgs e)
        {
            int index = selectedLineListbox.SelectedIndex;

            // Update the model
            Driver d = Driver.findDriver(selectedLineListbox.SelectedItem.ToString());
            selectedLine.Drivers.Remove(d);

            // Update listboxes
            selectedLineListbox.Items.RemoveAt(index);
            lsbAvailableDrivers.Items.Add(d.Name);

            UpdateState();
        }

        /// <summary>
        /// Affect all available drivers to the selected line
        /// </summary>
        private void btnAllRight_Click(object sender, EventArgs e)
        {
            // Add all the available drivers to the line
            foreach (var item in lsbAvailableDrivers.Items)
            {
                selectedLineListbox.Items.Add(item);
                selectedLine.Drivers.Add(Driver.findDriver(item.ToString()));
            }

            // Clear the available drivers listbox
            lsbAvailableDrivers.Items.Clear();

            UpdateState();
        }

        /// <summary>
        /// Set all drivers of a line to available
        /// </summary>
        private void btnAllLeft_Click(object sender, EventArgs e)
        {
            // Add all the drivers to the left listbox
            foreach (var item in selectedLineListbox.Items)
                lsbAvailableDrivers.Items.Add(item);

            // Clear the right listbox
            selectedLineListbox.Items.Clear();

            // Update the model
            selectedLine.Drivers.Clear();
            
            UpdateState();
        }

        /// <summary>
        /// Change the current line
        /// </summary>
        private void tcLines_SelectedIndexChanged(object sender, EventArgs e)
        {
            UpdateState();
        }
    }
}
