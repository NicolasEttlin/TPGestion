﻿//=========================================================================
// TPGESTION
// Small gestion game where you lead a public transit company
// Author            : Nicolas Ettlin <nicolas.ettln@eduge.ch>
// Last modification : November 29th 2017
//=========================================================================


using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace TPGestion
{
    static class Program
    {
        /// <summary>
        /// Point d'entrée principal de l'application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Models.Game.Initialize(); // Initialize the default game values
            Application.Run(new frmHome());
        }
    }
}
