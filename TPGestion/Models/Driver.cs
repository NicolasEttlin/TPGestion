﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TPGestion.Models
{
    /// <summary>
    /// Driver model
    /// </summary>
    public class Driver
    {
        /// <summary>
        /// Max drivers count
        /// </summary>
        public const int MAX_COUNT = 13 * 13;

        /// <summary>
        /// Full name
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Cast a line to string
        /// </summary>
        public override string ToString()
            => Name;

        /// <summary>
        /// Generate a random driver
        /// </summary>
        /// <param name="rnd">Random class instance</param>
        /// <returns>A new Driver instance</returns>
        public static Driver generateRandom()
        {
            string[] firstNames = {
                "Mayara", "Nicolas", "Sébastien", "Jules", "Ismaël", "Christian", "Qendrim", "Laetitia", "William", "Quentin", "Adamo", "Diogo", "Florian"
            };

            string[] lastNames = {
                "Cochard", "Ettlin", "Cuthbert", "Bursik", "Adda", "Da Silva Perez", "Bytyci", "Conus", "Dubelly", "Fasler", "Cannone", "Almeida", "Burgener"
            };

            string name;
            do
            {
                name = Rand.GetRandomElement(firstNames) + ' ' + Rand.GetRandomElement(lastNames);
            } while (findDriver(name) != null); // We must not have two drivers with the same name !

            return new Driver() {
                Name = name
            };
        }

        /// <summary>
        /// Generate a motivation letter
        /// </summary>
        /// <returns>Text</returns>
        public string getMotivationText()
        {
            string[] intros = { "Bonjour", "Madame, monsieur", "Cher madame, cher monsieur" };
            string[] motivations = { "votre compagnie est intéressante", "je cherche un travail", "j'aime beaucoup les bus", };
            string[] ends = { "Cordialement", "À bientôt", "Meilleures salutations", "À plus dans le bus", "En attendant votre réponse, je vous souhaite une agréable journée !", "Bonne journée" };

            return String.Format("{1},{0}Je m'appelle {2} et je postule au poste de conducteur dans votre entreprise car {3}.{0}{4}" + Environment.NewLine + Name,
                Environment.NewLine + Environment.NewLine,
                Rand.GetRandomElement(intros), Name, Rand.GetRandomElement(motivations), Rand.GetRandomElement(ends)
            );
        }

        /// <summary>
        /// Get the line where the driver is affected
        /// </summary>
        /// <returns>Line or null (if the driver is still available)</returns>
        public Line getLine()
        {
            foreach (Line l in Game.Lines)
                if (l.Drivers.Contains(this))
                    return l;

            return null;
        }

        /// <summary>
        /// Fire a driver
        /// </summary>
        public void Fire() {
            Game.Drivers.Remove(this);

            foreach (Line l in Game.Lines)
                l.Drivers.Remove(this);
        }

        /// <summary>
        /// Get the drivers without affected line
        /// </summary>
        public static List<Driver> GetAvailableDrivers()
            => Game.Drivers.Where(d => d.getLine() == null).ToList();

        /// <summary>
        /// Get all the drivers in service
        /// </summary>
        public static List<Driver> GetDriversInService()
            => Game.Drivers.Where(d => d.getLine() != null).ToList();

        /// <summary>
        /// Find a driver from a name
        /// </summary>
        /// <param name="fullName">Full driver name</param>
        /// <returns>Driver instance if found ; null otherwise</returns>
        public static Driver findDriver(string fullName)
        {
            List<Driver> drivers = Game.Drivers.Where(d => d.Name == fullName).ToList();

            if (drivers.Count == 0)
                return null;

            return drivers[0];
        }
    }
}
