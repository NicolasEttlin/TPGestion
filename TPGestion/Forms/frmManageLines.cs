﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

using TPGestion.Models;

namespace TPGestion.Forms
{
    public partial class frmManageLines : Form
    {
        private Line selectedLine;

        public frmManageLines()
        {
            InitializeComponent();
        }

        private void frmManageLines_Load(object sender, EventArgs e)
        {
            LoadLinesList();

            // Select the first driver
            lbxLines.SelectedIndex = 0;

            // Tooltip on driver name
            ToolTip t = new ToolTip();
            t.InitialDelay = 0;
            t.SetToolTip(lblLineName, "Cliquez ici pour renommer la ligne.");
        }

        /// <summary>
        /// Load the lines in the list box
        /// </summary>
        private void LoadLinesList()
        {
            lbxLines.Items.Clear();
            foreach (Line l in Game.Lines)
            {
                lbxLines.Items.Add(l.Resume);
            }
        }

        /// <summary>
        /// Create a new line
        /// </summary>
        private void btnNewLine_Click(object sender, EventArgs e)
        {
            frmNewLine f = new frmNewLine(Line.generateRandom());
            f.ShowDialog();

            if (f.DialogResult == DialogResult.Yes)
            {
                Game.Lines.Add(f.Line);
                LoadLinesList();

                // Select the new driver
                lbxLines.SelectedIndex = lbxLines.Items.Count - 1;
            }
        }

        /// <summary>
        /// Select a line in the list
        /// </summary>
        private void lbxLine_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (lbxLines.SelectedIndex == -1)
                return;

            selectedLine = Game.Lines[lbxLines.SelectedIndex];
            UpdateLineInfo();
        }

        private void UpdateLineInfo()
        {
            gbxLine.Enabled = true;
            gbxLine.Text = String.Format("Ligne {0}", selectedLine.Name);
            gbxLine.Visible = true;
            lblLineName.Text = $"Ligne {selectedLine.Name}";
            trbFrequency.Value = selectedLine.DriversSlots;

            // Affectation info
            int driversCount = selectedLine.Drivers.Count;

            string text;
            if (driversCount == 0)
                text = "Aucun conducteur n'est affecté à cette ligne.";
            else if (driversCount == 1)
                text = "1 conducteur est affecté à cette ligne.";
            else
                text = $"{driversCount} conducteurs sont affectés à cette ligne.";

            lblAffectationInfo.Text = text;
        }

        /// <summary>
        /// Change the line frequency
        /// </summary>
        private void tbFrequency_ValueChanged(object sender, EventArgs e)
        {
            selectedLine.DriversSlots = trbFrequency.Value;
            lblFrequency.Text = trbFrequency.Value.ToString();
        }

        /// <summary>
        /// Open the affect drivers form
        /// </summary>
        private void btnEditAffectation_Click(object sender, EventArgs e)
        {
            (new frmAffectDrivers(selectedLine)).ShowDialog();
            UpdateLineInfo();
        }

        /// <summary>
        /// Rename the line
        /// </summary>
        private void lblLineName_Click(object sender, EventArgs e)
        {
            frmLineName frmLineName = new frmLineName(selectedLine.Name);
            frmLineName.ShowDialog();
            if (frmLineName.DialogResult == DialogResult.OK) // The line was renamed
            {
                selectedLine.Name = frmLineName.LineName;
                lbxLines.Items[lbxLines.SelectedIndex] = selectedLine.Resume;
                UpdateLineInfo();
            }
        }

        /// <summary>
        /// Delete the line
        /// </summary>
        private void btnDelete_Click(object sender, EventArgs e)
        {
            if (Game.Lines.Count <= 1) // The player must have at least one line
            {
                MessageBox.Show("Vous devez avoir au minimum une ligne.", "Erreur", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            Game.Lines.Remove(selectedLine);
            LoadLinesList();

            // Select the first line
            lbxLines.SelectedIndex = 0;
        }
    }
}
