﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

using TPGestion.Models;
using TPGestion.Forms;

namespace TPGestion
{
    /// <summary>
    /// Home screen
    /// </summary>
    public partial class frmHome : Form
    {
        private bool _forceClose = false;
        private static frmAbout frmAbout;

        public frmHome()
        {
            InitializeComponent();
        }

        private void frmHome_Load(object sender, EventArgs e)
        {
            RefreshData();
        }

        /// <summary>
        /// Refresh the form data (news box...)
        /// </summary>
        public void RefreshData()
        {
            // Infos text box content
            StringBuilder infos = new StringBuilder();
            infos.AppendLine($"JOUR #{Game.Day + 1}");
            infos.AppendLine(new string('-', 20));
            infos.AppendLine();

            bool validAffectation = true;
            foreach (Line l in Game.Lines)
            {
                if (l.Drivers.Count < l.DriversSlots)
                {
                    infos.AppendLine($"La ligne {l.Name} n'a pas assez de conducteurs affectés, vous devez affecter du personnel supplémentaire ou diminuer la fréquence de la ligne pour pouvoir continuer.");
                    validAffectation = false;
                }
                else if (l.Drivers.Count > l.DriversSlots)
                {
                    infos.AppendLine($"La ligne {l.Name} a trop de conducteurs affectés, vous devez y désaffecter du personnel ou augmenter sa fréquence pour pouvoir continuer.");
                    validAffectation = false;
                }
            }

            tbxInfos.Text = infos.ToString();
            btnNextDay.Enabled = validAffectation;
        }

        /// <summary>
        /// Form close
        /// </summary>
        private void frmHome_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (_forceClose)
            {
                return;
            }

            // Show a warning message then close the application
            if (MessageBox.Show("Voulez-vous vraiment quitter le jeu ? Vous perderez votre progression.", "Avertissement", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == DialogResult.No)
            {
                e.Cancel = true;
            }
            else
            {
                _forceClose = true;
                Application.Exit();
            }
        }

        #region Open forms buttons

        /// <summary>
        /// Open the vehicles repair minigame
        /// </summary>
        private void btnOpenVehicles_Click(object sender, EventArgs e)
        {
            (new frmRepairVehicle()).ShowDialog();
            RefreshData();
        }

        /// <summary>
        /// Open the drivers management form
        /// </summary>
        private void btnOpenDrivers_Click(object sender, EventArgs e)
        {
            (new frmManageDrivers()).ShowDialog();
            RefreshData();
        }

        /// <summary>
        /// Open the lines management form
        /// </summary>
        private void btnOpenLines_Click(object sender, EventArgs e)
        {
            (new frmManageLines()).ShowDialog();
            RefreshData();
        }

        /// <summary>
        /// Open the about screen
        /// </summary>
        private void btnAbout_Click(object sender, EventArgs e)
        {
            if (frmAbout != null)
                frmAbout.Close();

            frmAbout = new frmAbout();
            frmAbout.Show();
            RefreshData();
        }

        /// <summary>
        /// Open the main game screen (next day)
        /// </summary>
        private void btnNextDay_Click(object sender, EventArgs e)
        {
            (new frmGame()).Show();
            Hide();
            _forceClose = true;
        }

        #endregion
    }
}
