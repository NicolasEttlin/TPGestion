﻿namespace TPGestion.Forms
{
    partial class frmRepairVehicle
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmRepairVehicle));
            this.pbxBackground = new System.Windows.Forms.PictureBox();
            this.btnRepair = new System.Windows.Forms.Button();
            this.lblProgress = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.pbxBackground)).BeginInit();
            this.SuspendLayout();
            // 
            // pbxBackground
            // 
            this.pbxBackground.BackColor = System.Drawing.Color.Black;
            this.pbxBackground.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pbxBackground.Image = ((System.Drawing.Image)(resources.GetObject("pbxBackground.Image")));
            this.pbxBackground.Location = new System.Drawing.Point(0, 0);
            this.pbxBackground.Name = "pbxBackground";
            this.pbxBackground.Size = new System.Drawing.Size(1205, 745);
            this.pbxBackground.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pbxBackground.TabIndex = 0;
            this.pbxBackground.TabStop = false;
            // 
            // btnRepair
            // 
            this.btnRepair.Location = new System.Drawing.Point(12, 394);
            this.btnRepair.Name = "btnRepair";
            this.btnRepair.Size = new System.Drawing.Size(90, 45);
            this.btnRepair.TabIndex = 0;
            this.btnRepair.TabStop = false;
            this.btnRepair.Text = "Réparer";
            this.btnRepair.UseVisualStyleBackColor = true;
            this.btnRepair.Click += new System.EventHandler(this.btnRepair_Click);
            this.btnRepair.MouseLeave += new System.EventHandler(this.btnRepair_MouseLeave);
            this.btnRepair.MouseHover += new System.EventHandler(this.btnRepair_MouseHover);
            // 
            // lblProgress
            // 
            this.lblProgress.AutoSize = true;
            this.lblProgress.BackColor = System.Drawing.Color.Black;
            this.lblProgress.Font = new System.Drawing.Font("Arial", 48F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblProgress.ForeColor = System.Drawing.Color.White;
            this.lblProgress.Location = new System.Drawing.Point(12, 21);
            this.lblProgress.Name = "lblProgress";
            this.lblProgress.Size = new System.Drawing.Size(158, 75);
            this.lblProgress.TabIndex = 1;
            this.lblProgress.Text = "5/15";
            // 
            // frmRepairVehicle
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1205, 745);
            this.Controls.Add(this.lblProgress);
            this.Controls.Add(this.btnRepair);
            this.Controls.Add(this.pbxBackground);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.MaximizeBox = false;
            this.Name = "frmRepairVehicle";
            this.ShowIcon = false;
            this.Text = "Réparer un véhicule";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frmRepairVehicle_FormClosing);
            this.Load += new System.EventHandler(this.frmRepairVehicle_Load);
            ((System.ComponentModel.ISupportInitialize)(this.pbxBackground)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox pbxBackground;
        private System.Windows.Forms.Button btnRepair;
        private System.Windows.Forms.Label lblProgress;
    }
}