﻿namespace TPGestion.Forms
{
    partial class frmGame
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.tableLayoutPanel2 = new System.Windows.Forms.TableLayoutPanel();
            this.btnPreviousEvent = new System.Windows.Forms.Button();
            this.btnNextEvent = new System.Windows.Forms.Button();
            this.lblEvents = new System.Windows.Forms.Label();
            this.tbxEvent = new System.Windows.Forms.TextBox();
            this.gbxActions = new System.Windows.Forms.GroupBox();
            this.tableLayoutPanel4 = new System.Windows.Forms.TableLayoutPanel();
            this.rdbDoNothing = new System.Windows.Forms.RadioButton();
            this.rdbSendMessage = new System.Windows.Forms.RadioButton();
            this.btnFixEvent = new System.Windows.Forms.Button();
            this.rdbReplaceDriver = new System.Windows.Forms.RadioButton();
            this.tableLayoutPanel3 = new System.Windows.Forms.TableLayoutPanel();
            this.btnNextHour = new System.Windows.Forms.Button();
            this.lblHour = new System.Windows.Forms.Label();
            this.tableLayoutPanel1.SuspendLayout();
            this.tableLayoutPanel2.SuspendLayout();
            this.gbxActions.SuspendLayout();
            this.tableLayoutPanel4.SuspendLayout();
            this.tableLayoutPanel3.SuspendLayout();
            this.SuspendLayout();
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 1;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.Controls.Add(this.tableLayoutPanel2, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.tbxEvent, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.gbxActions, 0, 2);
            this.tableLayoutPanel1.Controls.Add(this.tableLayoutPanel3, 0, 3);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 4;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 7.909105F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 65.75448F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 16.20703F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 10.12939F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(948, 582);
            this.tableLayoutPanel1.TabIndex = 0;
            // 
            // tableLayoutPanel2
            // 
            this.tableLayoutPanel2.ColumnCount = 3;
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 23.68421F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 52.63158F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 23.68421F));
            this.tableLayoutPanel2.Controls.Add(this.btnPreviousEvent, 0, 0);
            this.tableLayoutPanel2.Controls.Add(this.btnNextEvent, 2, 0);
            this.tableLayoutPanel2.Controls.Add(this.lblEvents, 1, 0);
            this.tableLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel2.Location = new System.Drawing.Point(3, 3);
            this.tableLayoutPanel2.Name = "tableLayoutPanel2";
            this.tableLayoutPanel2.RowCount = 1;
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel2.Size = new System.Drawing.Size(942, 40);
            this.tableLayoutPanel2.TabIndex = 0;
            // 
            // btnPreviousEvent
            // 
            this.btnPreviousEvent.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnPreviousEvent.Enabled = false;
            this.btnPreviousEvent.Location = new System.Drawing.Point(3, 3);
            this.btnPreviousEvent.Name = "btnPreviousEvent";
            this.btnPreviousEvent.Size = new System.Drawing.Size(217, 34);
            this.btnPreviousEvent.TabIndex = 0;
            this.btnPreviousEvent.Text = "<<";
            this.btnPreviousEvent.UseVisualStyleBackColor = true;
            this.btnPreviousEvent.Click += new System.EventHandler(this.btnsEventsNavigation_Click);
            // 
            // btnNextEvent
            // 
            this.btnNextEvent.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnNextEvent.Location = new System.Drawing.Point(721, 3);
            this.btnNextEvent.Name = "btnNextEvent";
            this.btnNextEvent.Size = new System.Drawing.Size(218, 34);
            this.btnNextEvent.TabIndex = 1;
            this.btnNextEvent.Text = ">>";
            this.btnNextEvent.UseVisualStyleBackColor = true;
            this.btnNextEvent.Click += new System.EventHandler(this.btnsEventsNavigation_Click);
            // 
            // lblEvents
            // 
            this.lblEvents.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblEvents.Font = new System.Drawing.Font("Arial", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblEvents.Location = new System.Drawing.Point(226, 0);
            this.lblEvents.Name = "lblEvents";
            this.lblEvents.Size = new System.Drawing.Size(489, 40);
            this.lblEvents.TabIndex = 2;
            this.lblEvents.Text = "Incident 1/2";
            this.lblEvents.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // tbxEvent
            // 
            this.tbxEvent.BackColor = System.Drawing.Color.White;
            this.tbxEvent.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tbxEvent.Font = new System.Drawing.Font("Arial", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbxEvent.Location = new System.Drawing.Point(3, 49);
            this.tbxEvent.Multiline = true;
            this.tbxEvent.Name = "tbxEvent";
            this.tbxEvent.ReadOnly = true;
            this.tbxEvent.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.tbxEvent.Size = new System.Drawing.Size(942, 376);
            this.tbxEvent.TabIndex = 1;
            this.tbxEvent.Text = "...";
            // 
            // gbxActions
            // 
            this.gbxActions.Controls.Add(this.tableLayoutPanel4);
            this.gbxActions.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gbxActions.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gbxActions.Location = new System.Drawing.Point(3, 431);
            this.gbxActions.Name = "gbxActions";
            this.gbxActions.Size = new System.Drawing.Size(942, 88);
            this.gbxActions.TabIndex = 2;
            this.gbxActions.TabStop = false;
            this.gbxActions.Text = "Comment souhaitez-vous réagir ?";
            // 
            // tableLayoutPanel4
            // 
            this.tableLayoutPanel4.ColumnCount = 4;
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 30.1282F));
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 27.67094F));
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 19.44444F));
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 22.65193F));
            this.tableLayoutPanel4.Controls.Add(this.rdbDoNothing, 2, 0);
            this.tableLayoutPanel4.Controls.Add(this.rdbSendMessage, 1, 0);
            this.tableLayoutPanel4.Controls.Add(this.btnFixEvent, 3, 0);
            this.tableLayoutPanel4.Controls.Add(this.rdbReplaceDriver, 0, 0);
            this.tableLayoutPanel4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel4.Location = new System.Drawing.Point(3, 22);
            this.tableLayoutPanel4.Margin = new System.Windows.Forms.Padding(3, 10, 3, 10);
            this.tableLayoutPanel4.Name = "tableLayoutPanel4";
            this.tableLayoutPanel4.RowCount = 1;
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel4.Size = new System.Drawing.Size(936, 63);
            this.tableLayoutPanel4.TabIndex = 0;
            // 
            // rdbDoNothing
            // 
            this.rdbDoNothing.AutoSize = true;
            this.rdbDoNothing.Dock = System.Windows.Forms.DockStyle.Fill;
            this.rdbDoNothing.Location = new System.Drawing.Point(544, 3);
            this.rdbDoNothing.Name = "rdbDoNothing";
            this.rdbDoNothing.Padding = new System.Windows.Forms.Padding(15);
            this.rdbDoNothing.Size = new System.Drawing.Size(176, 57);
            this.rdbDoNothing.TabIndex = 3;
            this.rdbDoNothing.TabStop = true;
            this.rdbDoNothing.Tag = "";
            this.rdbDoNothing.Text = "Ne rien faire";
            this.rdbDoNothing.UseVisualStyleBackColor = true;
            this.rdbDoNothing.CheckedChanged += new System.EventHandler(this.rdbAction_CheckedChanged);
            // 
            // rdbSendMessage
            // 
            this.rdbSendMessage.AutoSize = true;
            this.rdbSendMessage.Dock = System.Windows.Forms.DockStyle.Fill;
            this.rdbSendMessage.Location = new System.Drawing.Point(285, 3);
            this.rdbSendMessage.Name = "rdbSendMessage";
            this.rdbSendMessage.Padding = new System.Windows.Forms.Padding(15);
            this.rdbSendMessage.Size = new System.Drawing.Size(253, 57);
            this.rdbSendMessage.TabIndex = 2;
            this.rdbSendMessage.TabStop = true;
            this.rdbSendMessage.Tag = "message";
            this.rdbSendMessage.Text = "Informer les passagers";
            this.rdbSendMessage.UseVisualStyleBackColor = true;
            this.rdbSendMessage.CheckedChanged += new System.EventHandler(this.rdbAction_CheckedChanged);
            // 
            // btnFixEvent
            // 
            this.btnFixEvent.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnFixEvent.Enabled = false;
            this.btnFixEvent.Location = new System.Drawing.Point(733, 10);
            this.btnFixEvent.Margin = new System.Windows.Forms.Padding(10);
            this.btnFixEvent.Name = "btnFixEvent";
            this.btnFixEvent.Size = new System.Drawing.Size(193, 43);
            this.btnFixEvent.TabIndex = 0;
            this.btnFixEvent.Text = "Effectuer cette action";
            this.btnFixEvent.UseVisualStyleBackColor = true;
            this.btnFixEvent.Click += new System.EventHandler(this.btnFixEvent_Click);
            // 
            // rdbReplaceDriver
            // 
            this.rdbReplaceDriver.AutoSize = true;
            this.rdbReplaceDriver.Dock = System.Windows.Forms.DockStyle.Fill;
            this.rdbReplaceDriver.Location = new System.Drawing.Point(3, 3);
            this.rdbReplaceDriver.Name = "rdbReplaceDriver";
            this.rdbReplaceDriver.Padding = new System.Windows.Forms.Padding(15);
            this.rdbReplaceDriver.Size = new System.Drawing.Size(276, 57);
            this.rdbReplaceDriver.TabIndex = 1;
            this.rdbReplaceDriver.TabStop = true;
            this.rdbReplaceDriver.Tag = "replace";
            this.rdbReplaceDriver.Text = "Remplacer le conducteur";
            this.rdbReplaceDriver.UseVisualStyleBackColor = true;
            this.rdbReplaceDriver.CheckedChanged += new System.EventHandler(this.rdbAction_CheckedChanged);
            // 
            // tableLayoutPanel3
            // 
            this.tableLayoutPanel3.BackColor = System.Drawing.Color.OrangeRed;
            this.tableLayoutPanel3.ColumnCount = 2;
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 69.21444F));
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 30.78556F));
            this.tableLayoutPanel3.Controls.Add(this.btnNextHour, 1, 0);
            this.tableLayoutPanel3.Controls.Add(this.lblHour, 0, 0);
            this.tableLayoutPanel3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel3.Location = new System.Drawing.Point(3, 525);
            this.tableLayoutPanel3.Name = "tableLayoutPanel3";
            this.tableLayoutPanel3.RowCount = 1;
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel3.Size = new System.Drawing.Size(942, 54);
            this.tableLayoutPanel3.TabIndex = 3;
            // 
            // btnNextHour
            // 
            this.btnNextHour.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnNextHour.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnNextHour.Location = new System.Drawing.Point(655, 3);
            this.btnNextHour.Name = "btnNextHour";
            this.btnNextHour.Size = new System.Drawing.Size(284, 48);
            this.btnNextHour.TabIndex = 0;
            this.btnNextHour.Text = "Heure suivante";
            this.btnNextHour.UseVisualStyleBackColor = true;
            this.btnNextHour.Click += new System.EventHandler(this.btnNextHour_Click);
            // 
            // lblHour
            // 
            this.lblHour.AutoSize = true;
            this.lblHour.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblHour.Font = new System.Drawing.Font("Arial Black", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblHour.ForeColor = System.Drawing.Color.White;
            this.lblHour.Location = new System.Drawing.Point(3, 0);
            this.lblHour.Name = "lblHour";
            this.lblHour.Size = new System.Drawing.Size(646, 54);
            this.lblHour.TabIndex = 1;
            this.lblHour.Text = "09:00";
            this.lblHour.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // frmGame
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(948, 582);
            this.Controls.Add(this.tableLayoutPanel1);
            this.MinimumSize = new System.Drawing.Size(950, 621);
            this.Name = "frmGame";
            this.ShowIcon = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Jour 1";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frmGame_FormClosing);
            this.Load += new System.EventHandler(this.frmGame_Load);
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel1.PerformLayout();
            this.tableLayoutPanel2.ResumeLayout(false);
            this.gbxActions.ResumeLayout(false);
            this.tableLayoutPanel4.ResumeLayout(false);
            this.tableLayoutPanel4.PerformLayout();
            this.tableLayoutPanel3.ResumeLayout(false);
            this.tableLayoutPanel3.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel2;
        private System.Windows.Forms.Button btnPreviousEvent;
        private System.Windows.Forms.Button btnNextEvent;
        private System.Windows.Forms.Label lblEvents;
        private System.Windows.Forms.TextBox tbxEvent;
        private System.Windows.Forms.GroupBox gbxActions;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel3;
        private System.Windows.Forms.Button btnNextHour;
        private System.Windows.Forms.Label lblHour;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel4;
        private System.Windows.Forms.Button btnFixEvent;
        private System.Windows.Forms.RadioButton rdbDoNothing;
        private System.Windows.Forms.RadioButton rdbSendMessage;
        private System.Windows.Forms.RadioButton rdbReplaceDriver;
    }
}