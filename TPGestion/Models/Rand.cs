﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TPGestion.Models
{
    /// <summary>
    /// Random class singleton
    /// </summary>
    static class Rand
    {
        private static Random _rnd = new Random();

        /// <summary>
        /// Returns a random positive integer
        /// </summary>
        public static int Next()
            => _rnd.Next();

        /// <summary>
        /// Returns a random integer smaller than maxValue
        /// </summary>
        /// <param name="maxValue">Maximum value (exclusive)</param>
        public static int Next(int maxValue)
            => _rnd.Next(maxValue);

        /// <summary>
        /// Returns a random integer in a range
        /// </summary>
        /// <param name="minValue">Minimum value (inclusive)</param>
        /// <param name="maxValue">Maximum value (exclusive)</param>
        /// <returns></returns>
        public static int Next(int minValue, int maxValue)
            => _rnd.Next(minValue, maxValue);

        public static T GetRandomElement<T>(T[] array)
        {
            return array[Next(array.Length)];
        }

        public static T GetRandomElement<T> (List<T> list)
        {
            return list[Next(list.Count)];
        }
    }
}
