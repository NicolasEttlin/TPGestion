﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace TPGestion.Forms
{
    public partial class frmAbout : Form
    {
        public frmAbout()
        {
            InitializeComponent();
        }

        /// <summary>
        /// Load form and show version number
        /// </summary>
        private void frmAbout_Load(object sender, EventArgs e)
        {
            lblSubtitle.Text = String.Format("Version {0} - © 2017 Nicolas Ettlin (classe I.FA-P2A)", Application.ProductVersion);
        }
    }
}
