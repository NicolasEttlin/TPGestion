﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

using TPGestion.Models;

namespace TPGestion.Forms
{
    public partial class frmNewDriver : Form
    {
        private Driver driver;

        public Driver Driver => driver;

        public frmNewDriver(Driver driver)
        {
            this.driver = driver;

            InitializeComponent();
        }

        private void frmNewDriver_Load(object sender, EventArgs e)
        {
            string[] fonts = { "Comic Sans MS", "Segoe Script", "Segoe Print" };

            this.Text = driver.Name;
            lblName.Text = driver.Name;
            tbxMotivationText.Text = driver.getMotivationText();
            tbxMotivationText.Font = new Font(Rand.GetRandomElement(fonts) as string, 18);
        }

        private void btnBuy_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.Yes;
            Close();
        }

        private void btnPass_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.Retry;
            Close();
        }
    }
}
