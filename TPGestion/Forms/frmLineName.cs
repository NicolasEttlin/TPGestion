﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace TPGestion.Forms
{
    public partial class frmLineName : Form
    {
        public string LineName
        {
            get
            {
                return tbxLineName.Text;
            }

            set
            {
                tbxLineName.Text = value;
            }
        }

        /// <summary>
        /// Initialize the form without a name
        /// </summary>
        public frmLineName()
        {
            InitializeComponent();
        }

        private string _prefilledText;

        /// <summary>
        /// Initialize the form with a name
        /// </summary>
        /// <param name="name"></param>
        public frmLineName(string name)
        {
            _prefilledText = name;
            InitializeComponent();
        }

        private void btnConfirm_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.OK;
            Close();
        }

        /// <summary>
        /// Disable button when the name is empty
        /// </summary>
        private void tbxLineName_TextChanged(object sender, EventArgs e)
        {
            tbxLineName.Text = tbxLineName.Text.ToUpper();

            // Can't use the "HP" name
            if (tbxLineName.Text == "HP")
            {
                MessageBox.Show("Le nom HP étant une marque déposée, vous ne pouvez pas l'utiliser.", "Erreur", MessageBoxButtons.OK, MessageBoxIcon.Error);
                tbxLineName.Clear();
            }

            btnConfirm.Enabled = tbxLineName.TextLength > 0;
        }

        /// <summary>
        /// Filter characters
        /// </summary>
        private void tbxLineName_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.KeyChar = char.ToUpper(e.KeyChar);
            e.Handled = !char.IsLetterOrDigit(e.KeyChar) && !char.IsControl(e.KeyChar);
        }

        /// <summary>
        /// Load the prefilled text
        /// </summary>
        private void frmLineName_Load(object sender, EventArgs e)
        {
            if (_prefilledText != null)
            {
                Text = "Renommer une ligne";
                LineName = _prefilledText;
            }
        }
    }
}
