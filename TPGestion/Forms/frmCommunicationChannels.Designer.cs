﻿namespace TPGestion.Forms
{
    partial class frmCommunicationChannels
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.chkTerminal = new System.Windows.Forms.CheckBox();
            this.chkWeb = new System.Windows.Forms.CheckBox();
            this.btnAccept = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // chkTerminal
            // 
            this.chkTerminal.Checked = true;
            this.chkTerminal.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chkTerminal.Location = new System.Drawing.Point(12, 12);
            this.chkTerminal.Name = "chkTerminal";
            this.chkTerminal.Size = new System.Drawing.Size(213, 24);
            this.chkTerminal.TabIndex = 0;
            this.chkTerminal.Text = "Bornes d\'information";
            this.chkTerminal.UseVisualStyleBackColor = true;
            this.chkTerminal.CheckedChanged += new System.EventHandler(this.chk_CheckedChanged);
            // 
            // chkWeb
            // 
            this.chkWeb.Location = new System.Drawing.Point(12, 42);
            this.chkWeb.Name = "chkWeb";
            this.chkWeb.Size = new System.Drawing.Size(213, 17);
            this.chkWeb.TabIndex = 1;
            this.chkWeb.Text = "Site web";
            this.chkWeb.UseVisualStyleBackColor = true;
            this.chkWeb.CheckedChanged += new System.EventHandler(this.chk_CheckedChanged);
            // 
            // btnAccept
            // 
            this.btnAccept.Location = new System.Drawing.Point(100, 65);
            this.btnAccept.Name = "btnAccept";
            this.btnAccept.Size = new System.Drawing.Size(125, 34);
            this.btnAccept.TabIndex = 2;
            this.btnAccept.Text = "Envoyer";
            this.btnAccept.UseVisualStyleBackColor = true;
            this.btnAccept.Click += new System.EventHandler(this.btnAccept_Click);
            // 
            // frmCommunicationChannels
            // 
            this.AcceptButton = this.btnAccept;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(237, 111);
            this.Controls.Add(this.btnAccept);
            this.Controls.Add(this.chkWeb);
            this.Controls.Add(this.chkTerminal);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmCommunicationChannels";
            this.ShowIcon = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Canaux de communication";
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.CheckBox chkTerminal;
        private System.Windows.Forms.CheckBox chkWeb;
        private System.Windows.Forms.Button btnAccept;
    }
}