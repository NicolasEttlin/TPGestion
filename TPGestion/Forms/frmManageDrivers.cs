﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

using TPGestion.Models;

namespace TPGestion.Forms
{
    public partial class frmManageDrivers : Form
    {
        private Driver selectedDriver;

        public frmManageDrivers()
        {
            InitializeComponent();
        }

        private void frmManageDrivers_Load(object sender, EventArgs e)
        {
            LoadDriversList();

            // Select the first driver
            lbxDrivers.SelectedIndex = 0;
        }

        /// <summary>
        /// Load the drivers in the list box
        /// </summary>
        private void LoadDriversList()
        {
            lbxDrivers.Items.Clear();
            foreach (Driver d in Game.Drivers)
            {
                lbxDrivers.Items.Add(d.Name);
            }
        }

        /// <summary>
        /// Hire new drivers
        /// </summary>
        private void btnNewDriver_Click(object sender, EventArgs e)
        {
            // Prevent the player from adding to many drivers
            if (Game.Drivers.Count >= (Driver.MAX_COUNT - 1))
            {
                MessageBox.Show("Vous avez déjà ajouté le nombre maximal de conducteurs.", "Erreur", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            frmNewDriver f = new frmNewDriver(Driver.generateRandom());
            f.ShowDialog();

            if (f.DialogResult == DialogResult.Retry) // Pass
            {
                btnNewDriver_Click(sender, e);
            }

            if (f.DialogResult == DialogResult.Yes) // Buy
            {
                Game.Drivers.Add(f.Driver);
                LoadDriversList();

                // Select the new driver
                lbxDrivers.SelectedIndex = lbxDrivers.Items.Count - 1;
            }
        }

        /// <summary>
        /// Select a driver in the list
        /// </summary>
        private void lbxDrivers_SelectedIndexChanged(object sender, EventArgs e)
        {
            selectedDriver = Game.Drivers[lbxDrivers.SelectedIndex];
            lblDriverName.Text = selectedDriver.Name;
            gbxDriver.Enabled = true;
            gbxDriver.Text = String.Format("Informations sur {0}", selectedDriver.Name);

            UpdateAffectationLabel();
        }

        /// <summary>
        /// Update lblAffectation
        /// </summary>
        private void UpdateAffectationLabel()
        {
            Line l = selectedDriver.getLine();
            lblAffectation.Text = (l == null) ? "Ce conducteur n'est affecté à aucune ligne." : $"Ce conducteur est affecté à la ligne {l.Name}.";
        }

        /// <summary>
        /// Fire a driver
        /// </summary>
        private void btnFireDriver_Click(object sender, EventArgs e)
        {
            if (Game.Drivers.Count <= 1) // The player must have at least one driver
            {
                MessageBox.Show("Vous devez avoir au minimum un conducteur.", "Erreur", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            selectedDriver.Fire();
            LoadDriversList();

            // Select the first driver
            lbxDrivers.SelectedIndex = 0;
        }

        private void btnAffect_Click(object sender, EventArgs e)
        {
            (new frmAffectDrivers(selectedDriver)).ShowDialog();
            UpdateAffectationLabel();
        }
    }
}
