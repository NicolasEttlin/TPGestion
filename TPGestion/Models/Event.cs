﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace TPGestion.Models
{
    /// <summary>
    /// Game event (disruption)
    /// </summary>
    class Event
    {
        public string Description { get; private set; }

        public string Message { get; private set; }

        public Driver Driver { get; set; }

        /// <summary>
        /// Get the message formatted for information terminals
        /// </summary>
        public string MessageTerminal
        {
            get
            {
                string message = Message.Replace(".", "");
                message = new Regex(@" (d\'une?|de|des|une?) ").Replace(message, " ");
                return message.ToUpper();
            }
        }

        /// <summary>
        /// Get the message formatted in HTML
        /// </summary>
        public string MessageWeb
        {
            get
            {
                return String.Format("<h1>Perturbation</h1><p>{0}</p>", Message);
            }
        }

        /// <summary>
        /// Constructor
        /// </summary>
        public Event() { }

        /// <summary>
        /// Generate a random event
        /// </summary>
        /// <param name="driver">The driver implied. Can be null</param>
        public static Event GenerateRandomEvent(Driver driver)
        {
            Event e = new Event();

            if (driver == null) {
                string[] descriptions = new string[] { "La machine à café est en panne.", "Il fait froid et les routes sont gelées, les lignes ont du retard." };
                string[] messages = new string[] { "En raison de problèmes techniques, le réseau rencontre des problèmes en ce moment.", "À cause de mauvaises conditions météo, nos lignes subissent des retards." };
                int eventNo = Rand.Next(descriptions.Length);
                e.Description = descriptions[eventNo];
                e.Message = messages[eventNo];
                
            } else {
                string[] events = new string[] { $"Quelqu'un aurait vu {driver} ?", $"{driver} est malade et ne peut pas prendre son service sur la ligne {driver.getLine()}.", $"La mère de l'oncle du parrain du meilleur ami de {driver} est malade. {driver} a quitté son service pour aller lui faire une tisane." };
                e.Description = Rand.GetRandomElement(events);
                e.Driver = driver;
                e.Message = $"En raison d'un problème de personnel, la ligne {driver.getLine()} subit des retards.";
            }

            return e;
        }
    }
}
