﻿namespace TPGestion
{
    partial class frmHome
    {
        /// <summary>
        /// Variable nécessaire au concepteur.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Nettoyage des ressources utilisées.
        /// </summary>
        /// <param name="disposing">true si les ressources managées doivent être supprimées ; sinon, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Code généré par le Concepteur Windows Form

        /// <summary>
        /// Méthode requise pour la prise en charge du concepteur - ne modifiez pas
        /// le contenu de cette méthode avec l'éditeur de code.
        /// </summary>
        private void InitializeComponent()
        {
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.tableLayoutPanel2 = new System.Windows.Forms.TableLayoutPanel();
            this.btnNextDay = new System.Windows.Forms.Button();
            this.tableLayoutPanel3 = new System.Windows.Forms.TableLayoutPanel();
            this.btnOpenLines = new System.Windows.Forms.Button();
            this.btnOpenVehicles = new System.Windows.Forms.Button();
            this.btnOpenDrivers = new System.Windows.Forms.Button();
            this.lblTitle = new System.Windows.Forms.Label();
            this.tbxInfos = new System.Windows.Forms.TextBox();
            this.btnAbout = new System.Windows.Forms.Button();
            this.tableLayoutPanel1.SuspendLayout();
            this.tableLayoutPanel2.SuspendLayout();
            this.tableLayoutPanel3.SuspendLayout();
            this.SuspendLayout();
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 1;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.Controls.Add(this.tableLayoutPanel2, 0, 2);
            this.tableLayoutPanel1.Controls.Add(this.lblTitle, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.tbxInfos, 0, 1);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 3;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 28.4819F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 38.18254F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33556F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(992, 706);
            this.tableLayoutPanel1.TabIndex = 0;
            // 
            // tableLayoutPanel2
            // 
            this.tableLayoutPanel2.ColumnCount = 2;
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel2.Controls.Add(this.btnNextDay, 0, 0);
            this.tableLayoutPanel2.Controls.Add(this.tableLayoutPanel3, 1, 0);
            this.tableLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel2.Location = new System.Drawing.Point(3, 473);
            this.tableLayoutPanel2.Name = "tableLayoutPanel2";
            this.tableLayoutPanel2.RowCount = 1;
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel2.Size = new System.Drawing.Size(986, 230);
            this.tableLayoutPanel2.TabIndex = 3;
            // 
            // btnNextDay
            // 
            this.btnNextDay.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnNextDay.Enabled = false;
            this.btnNextDay.Font = new System.Drawing.Font("Arial", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnNextDay.Location = new System.Drawing.Point(6, 6);
            this.btnNextDay.Margin = new System.Windows.Forms.Padding(6, 6, 0, 6);
            this.btnNextDay.Name = "btnNextDay";
            this.btnNextDay.Size = new System.Drawing.Size(487, 218);
            this.btnNextDay.TabIndex = 0;
            this.btnNextDay.Text = "&Prochaine journée";
            this.btnNextDay.UseVisualStyleBackColor = true;
            this.btnNextDay.Click += new System.EventHandler(this.btnNextDay_Click);
            // 
            // tableLayoutPanel3
            // 
            this.tableLayoutPanel3.ColumnCount = 1;
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel3.Controls.Add(this.btnOpenLines, 0, 1);
            this.tableLayoutPanel3.Controls.Add(this.btnOpenVehicles, 0, 1);
            this.tableLayoutPanel3.Controls.Add(this.btnOpenDrivers, 0, 0);
            this.tableLayoutPanel3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel3.Location = new System.Drawing.Point(496, 3);
            this.tableLayoutPanel3.Name = "tableLayoutPanel3";
            this.tableLayoutPanel3.RowCount = 2;
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel3.Size = new System.Drawing.Size(487, 224);
            this.tableLayoutPanel3.TabIndex = 1;
            // 
            // btnOpenLines
            // 
            this.btnOpenLines.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnOpenLines.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnOpenLines.Location = new System.Drawing.Point(3, 77);
            this.btnOpenLines.Name = "btnOpenLines";
            this.btnOpenLines.Size = new System.Drawing.Size(481, 68);
            this.btnOpenLines.TabIndex = 1;
            this.btnOpenLines.Text = "Gérer les &lignes";
            this.btnOpenLines.UseVisualStyleBackColor = true;
            this.btnOpenLines.Click += new System.EventHandler(this.btnOpenLines_Click);
            // 
            // btnOpenVehicles
            // 
            this.btnOpenVehicles.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnOpenVehicles.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnOpenVehicles.Location = new System.Drawing.Point(3, 151);
            this.btnOpenVehicles.Name = "btnOpenVehicles";
            this.btnOpenVehicles.Size = new System.Drawing.Size(481, 70);
            this.btnOpenVehicles.TabIndex = 2;
            this.btnOpenVehicles.Text = "Mini-jeu : réparer les &véhicules";
            this.btnOpenVehicles.UseVisualStyleBackColor = true;
            this.btnOpenVehicles.Click += new System.EventHandler(this.btnOpenVehicles_Click);
            // 
            // btnOpenDrivers
            // 
            this.btnOpenDrivers.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnOpenDrivers.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnOpenDrivers.Location = new System.Drawing.Point(3, 3);
            this.btnOpenDrivers.Name = "btnOpenDrivers";
            this.btnOpenDrivers.Size = new System.Drawing.Size(481, 68);
            this.btnOpenDrivers.TabIndex = 0;
            this.btnOpenDrivers.Text = "Gérer les &conducteurs";
            this.btnOpenDrivers.UseVisualStyleBackColor = true;
            this.btnOpenDrivers.Click += new System.EventHandler(this.btnOpenDrivers_Click);
            // 
            // lblTitle
            // 
            this.lblTitle.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblTitle.Font = new System.Drawing.Font("Arial", 48F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTitle.ForeColor = System.Drawing.Color.OrangeRed;
            this.lblTitle.Location = new System.Drawing.Point(3, 0);
            this.lblTitle.Name = "lblTitle";
            this.lblTitle.Size = new System.Drawing.Size(986, 201);
            this.lblTitle.TabIndex = 0;
            this.lblTitle.Text = "TPGestion";
            this.lblTitle.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // tbxInfos
            // 
            this.tbxInfos.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(224)))), ((int)(((byte)(192)))));
            this.tbxInfos.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tbxInfos.Font = new System.Drawing.Font("Consolas", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbxInfos.ForeColor = System.Drawing.Color.OrangeRed;
            this.tbxInfos.Location = new System.Drawing.Point(6, 204);
            this.tbxInfos.Margin = new System.Windows.Forms.Padding(6, 3, 6, 3);
            this.tbxInfos.MaxLength = 500;
            this.tbxInfos.Multiline = true;
            this.tbxInfos.Name = "tbxInfos";
            this.tbxInfos.ReadOnly = true;
            this.tbxInfos.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.tbxInfos.Size = new System.Drawing.Size(980, 263);
            this.tbxInfos.TabIndex = 1;
            this.tbxInfos.TabStop = false;
            this.tbxInfos.Text = "Jour X - informations sur le jeu";
            // 
            // btnAbout
            // 
            this.btnAbout.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnAbout.Font = new System.Drawing.Font("Arial Narrow", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAbout.Location = new System.Drawing.Point(877, 10);
            this.btnAbout.Name = "btnAbout";
            this.btnAbout.Size = new System.Drawing.Size(104, 34);
            this.btnAbout.TabIndex = 1;
            this.btnAbout.Text = "À propos";
            this.btnAbout.UseVisualStyleBackColor = true;
            this.btnAbout.Click += new System.EventHandler(this.btnAbout_Click);
            // 
            // frmHome
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(992, 706);
            this.Controls.Add(this.btnAbout);
            this.Controls.Add(this.tableLayoutPanel1);
            this.MinimumSize = new System.Drawing.Size(600, 400);
            this.Name = "frmHome";
            this.ShowIcon = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "TPGestion";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frmHome_FormClosing);
            this.Load += new System.EventHandler(this.frmHome_Load);
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel1.PerformLayout();
            this.tableLayoutPanel2.ResumeLayout(false);
            this.tableLayoutPanel3.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel2;
        private System.Windows.Forms.Button btnNextDay;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel3;
        private System.Windows.Forms.Button btnOpenDrivers;
        private System.Windows.Forms.Label lblTitle;
        private System.Windows.Forms.TextBox tbxInfos;
        private System.Windows.Forms.Button btnOpenLines;
        private System.Windows.Forms.Button btnOpenVehicles;
        private System.Windows.Forms.Button btnAbout;
    }
}

