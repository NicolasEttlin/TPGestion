﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace TPGestion.Forms
{
    /// <summary>
    /// Choose the ways we should send a message
    /// </summary>
    public partial class frmCommunicationChannels : Form
    {
        public frmCommunicationChannels()
        {
            InitializeComponent();
        }

        /// <summary>
        /// Is web checked
        /// </summary>
        public bool WebEnabled
        {
            get
            {
                return chkWeb.Checked;
            }
        }

        /// <summary>
        /// Is terminal checked
        /// </summary>
        public bool TerminalEnabled
        {
            get
            {
                return chkTerminal.Checked;
            }
        }

        /// <summary>
        /// Accept button
        /// </summary>
        private void btnAccept_Click(object sender, EventArgs e)
        {
            Close();
        }

        /// <summary>
        /// We should have at least one checkbox checked.
        /// </summary>
        private void chk_CheckedChanged(object sender, EventArgs e)
        {
            CheckBox chk = sender as CheckBox;

            if (!chk.Checked)
            {
                CheckBox otherCheckbox = Controls.OfType<CheckBox>().Where(c => c != chk).First();
                otherCheckbox.Checked = true;
            }
        }
    }
}
