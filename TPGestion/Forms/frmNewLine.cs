﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

using TPGestion.Models;

namespace TPGestion.Forms
{
    partial class frmNewLine : Form
    {
        private Line _line;

        internal Line Line => _line;

        public frmNewLine(Line line)
        {
            _line = line;
            InitializeComponent();
        }

        /// <summary>
        /// Show the line details after the wait
        /// </summary>
        private void tmrShow_Tick(object sender, EventArgs e)
        {
            tmrShow.Enabled = false;
            tbxDescription.Text = Line.getPresentationText();
            tlpPresentation.Enabled = true;
        }

        /// <summary>
        /// Go to a section
        /// </summary>
        private void lbxNavigation_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (lbxNavigation.SelectedIndex == -1)
                return;

            string titleName = lbxNavigation.GetItemText(lbxNavigation.SelectedItem);
            int pos = tbxDescription.Text.IndexOf(titleName.ToUpper());

            // Scroll to the title pos
            tbxDescription.SelectionStart = pos + titleName.Length +  30;
            tbxDescription.ScrollToCaret();

            lbxNavigation.SelectedIndex = -1;
        }

        /// <summary>
        /// Add the line to the network
        /// </summary>
        private void btnAdd_Click(object sender, EventArgs e)
        {
            frmLineName fln = new frmLineName();
            fln.ShowDialog();

            if (fln.DialogResult != DialogResult.OK)
            {
                DialogResult errorResult = MessageBox.Show("Vous devez entrer un nom de ligne pour continuer", "Saisie invalide", MessageBoxButtons.RetryCancel);

                if (errorResult == DialogResult.Retry)
                {
                    btnAdd_Click(sender, e);
                }

                if (errorResult == DialogResult.Cancel)
                {
                    Close();
                }
            }
            else // Valid name
            {
                Line.Name = fln.LineName;
                DialogResult = DialogResult.Yes;
                Close();
            }
        }
    }
}
