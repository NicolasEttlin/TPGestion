﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using TPGestion.Models;

namespace TPGestion.Forms
{
    /// <summary>
    /// Form to choose an available driver
    /// </summary>
    public partial class frmChooseAvailableDriver : Form
    {
        public frmChooseAvailableDriver()
        {
            InitializeComponent();
        }

        /// <summary>
        /// Selected driver
        /// </summary>
        public Driver SelectedDriver
        {
            get
            {
                return Driver.findDriver(cmbDrivers.Text);
            }
        }

        /// <summary>
        /// Fill the drivers list
        /// </summary>
        private void frmChooseAvailableDriver_Load(object sender, EventArgs e)
        {
            List<Driver> drivers = Driver.GetAvailableDrivers();

            if (drivers.Count == 0)
            {
                MessageBox.Show("Aucun conducteur n'est disponible", "Erreur", MessageBoxButtons.OK, MessageBoxIcon.Error);
                Close();
                return;
            }

            foreach (Driver d in drivers)
                cmbDrivers.Items.Add(d.Name);

            cmbDrivers.SelectedIndex = 0; // Select the first driver
        }

        /// <summary>
        /// Enable the button when the user selects a driver
        /// </summary>
        private void cmbDrivers_SelectedIndexChanged(object sender, EventArgs e) =>
            btnAccept.Enabled = true;

        /// <summary>
        /// OK button
        /// </summary>
        private void btnAccept_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.OK;
            Close();
        }
    }
}
