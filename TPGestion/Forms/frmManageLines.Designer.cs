﻿namespace TPGestion.Forms
{
    partial class frmManageLines
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.lbxLines = new System.Windows.Forms.ListBox();
            this.btnNewLine = new System.Windows.Forms.Button();
            this.gbxLine = new System.Windows.Forms.GroupBox();
            this.tableLayoutPanel2 = new System.Windows.Forms.TableLayoutPanel();
            this.label1 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.btnDelete = new System.Windows.Forms.Button();
            this.lblLineName = new System.Windows.Forms.Label();
            this.tableLayoutPanel3 = new System.Windows.Forms.TableLayoutPanel();
            this.trbFrequency = new System.Windows.Forms.TrackBar();
            this.lblFrequency = new System.Windows.Forms.Label();
            this.btnEditAffectation = new System.Windows.Forms.Button();
            this.lblAffectationInfo = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            this.tableLayoutPanel1.SuspendLayout();
            this.gbxLine.SuspendLayout();
            this.tableLayoutPanel2.SuspendLayout();
            this.tableLayoutPanel3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.trbFrequency)).BeginInit();
            this.SuspendLayout();
            // 
            // splitContainer1
            // 
            this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer1.Location = new System.Drawing.Point(0, 0);
            this.splitContainer1.Name = "splitContainer1";
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add(this.tableLayoutPanel1);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.Controls.Add(this.gbxLine);
            this.splitContainer1.Size = new System.Drawing.Size(784, 461);
            this.splitContainer1.SplitterDistance = 261;
            this.splitContainer1.TabIndex = 0;
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 1;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.Controls.Add(this.lbxLines, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.btnNewLine, 0, 1);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 2;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(261, 461);
            this.tableLayoutPanel1.TabIndex = 0;
            // 
            // lbxLines
            // 
            this.lbxLines.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lbxLines.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbxLines.FormattingEnabled = true;
            this.lbxLines.IntegralHeight = false;
            this.lbxLines.ItemHeight = 20;
            this.lbxLines.Location = new System.Drawing.Point(3, 3);
            this.lbxLines.Name = "lbxLines";
            this.lbxLines.Size = new System.Drawing.Size(255, 405);
            this.lbxLines.TabIndex = 0;
            this.lbxLines.SelectedIndexChanged += new System.EventHandler(this.lbxLine_SelectedIndexChanged);
            // 
            // btnNewLine
            // 
            this.btnNewLine.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnNewLine.Location = new System.Drawing.Point(3, 414);
            this.btnNewLine.Name = "btnNewLine";
            this.btnNewLine.Size = new System.Drawing.Size(255, 44);
            this.btnNewLine.TabIndex = 1;
            this.btnNewLine.Text = "&Nouvelle ligne";
            this.btnNewLine.UseVisualStyleBackColor = true;
            this.btnNewLine.Click += new System.EventHandler(this.btnNewLine_Click);
            // 
            // gbxLine
            // 
            this.gbxLine.Controls.Add(this.tableLayoutPanel2);
            this.gbxLine.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gbxLine.Enabled = false;
            this.gbxLine.Location = new System.Drawing.Point(0, 0);
            this.gbxLine.Name = "gbxLine";
            this.gbxLine.Size = new System.Drawing.Size(519, 461);
            this.gbxLine.TabIndex = 0;
            this.gbxLine.TabStop = false;
            this.gbxLine.Text = "Ligne XX";
            this.gbxLine.Visible = false;
            // 
            // tableLayoutPanel2
            // 
            this.tableLayoutPanel2.ColumnCount = 1;
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel2.Controls.Add(this.label1, 0, 3);
            this.tableLayoutPanel2.Controls.Add(this.label4, 0, 1);
            this.tableLayoutPanel2.Controls.Add(this.btnDelete, 0, 6);
            this.tableLayoutPanel2.Controls.Add(this.lblLineName, 0, 0);
            this.tableLayoutPanel2.Controls.Add(this.tableLayoutPanel3, 0, 2);
            this.tableLayoutPanel2.Controls.Add(this.btnEditAffectation, 0, 5);
            this.tableLayoutPanel2.Controls.Add(this.lblAffectationInfo, 0, 4);
            this.tableLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel2.Location = new System.Drawing.Point(3, 16);
            this.tableLayoutPanel2.Name = "tableLayoutPanel2";
            this.tableLayoutPanel2.RowCount = 8;
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 65F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 17.72987F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 17.72987F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 17.72987F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 11.35066F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 17.72987F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 17.72987F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.tableLayoutPanel2.Size = new System.Drawing.Size(513, 442);
            this.tableLayoutPanel2.TabIndex = 0;
            // 
            // label1
            // 
            this.label1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.label1.Font = new System.Drawing.Font("Segoe UI", 14.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(3, 205);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(507, 31);
            this.label1.TabIndex = 2;
            this.label1.Text = "Affectation";
            this.label1.TextAlign = System.Drawing.ContentAlignment.BottomLeft;
            // 
            // label4
            // 
            this.label4.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.label4.Font = new System.Drawing.Font("Segoe UI", 14.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(3, 91);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(507, 31);
            this.label4.TabIndex = 1;
            this.label4.Text = "Nombre de véhicules";
            this.label4.TextAlign = System.Drawing.ContentAlignment.BottomLeft;
            // 
            // btnDelete
            // 
            this.btnDelete.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.btnDelete.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnDelete.ForeColor = System.Drawing.Color.Crimson;
            this.btnDelete.Location = new System.Drawing.Point(3, 350);
            this.btnDelete.Name = "btnDelete";
            this.btnDelete.Size = new System.Drawing.Size(507, 34);
            this.btnDelete.TabIndex = 5;
            this.btnDelete.Text = "&Supprimer";
            this.btnDelete.UseVisualStyleBackColor = true;
            this.btnDelete.Click += new System.EventHandler(this.btnDelete_Click);
            // 
            // lblLineName
            // 
            this.lblLineName.AutoSize = true;
            this.lblLineName.Dock = System.Windows.Forms.DockStyle.Top;
            this.lblLineName.Font = new System.Drawing.Font("Segoe UI Black", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblLineName.Location = new System.Drawing.Point(3, 0);
            this.lblLineName.Name = "lblLineName";
            this.lblLineName.Padding = new System.Windows.Forms.Padding(0, 0, 0, 20);
            this.lblLineName.Size = new System.Drawing.Size(507, 65);
            this.lblLineName.TabIndex = 0;
            this.lblLineName.Text = "Ligne XX";
            this.lblLineName.Click += new System.EventHandler(this.lblLineName_Click);
            // 
            // tableLayoutPanel3
            // 
            this.tableLayoutPanel3.ColumnCount = 2;
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.tableLayoutPanel3.Controls.Add(this.trbFrequency, 0, 0);
            this.tableLayoutPanel3.Controls.Add(this.lblFrequency, 1, 0);
            this.tableLayoutPanel3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel3.Location = new System.Drawing.Point(3, 125);
            this.tableLayoutPanel3.Name = "tableLayoutPanel3";
            this.tableLayoutPanel3.RowCount = 1;
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel3.Size = new System.Drawing.Size(507, 51);
            this.tableLayoutPanel3.TabIndex = 18;
            // 
            // trbFrequency
            // 
            this.trbFrequency.Dock = System.Windows.Forms.DockStyle.Fill;
            this.trbFrequency.Location = new System.Drawing.Point(3, 3);
            this.trbFrequency.Maximum = 20;
            this.trbFrequency.Minimum = 1;
            this.trbFrequency.Name = "trbFrequency";
            this.trbFrequency.Size = new System.Drawing.Size(451, 45);
            this.trbFrequency.TabIndex = 0;
            this.trbFrequency.TickFrequency = 5;
            this.trbFrequency.Value = 1;
            this.trbFrequency.ValueChanged += new System.EventHandler(this.tbFrequency_ValueChanged);
            // 
            // lblFrequency
            // 
            this.lblFrequency.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblFrequency.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblFrequency.Location = new System.Drawing.Point(460, 0);
            this.lblFrequency.Name = "lblFrequency";
            this.lblFrequency.Size = new System.Drawing.Size(44, 51);
            this.lblFrequency.TabIndex = 1;
            this.lblFrequency.Text = "1";
            this.lblFrequency.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // btnEditAffectation
            // 
            this.btnEditAffectation.Location = new System.Drawing.Point(3, 276);
            this.btnEditAffectation.Name = "btnEditAffectation";
            this.btnEditAffectation.Size = new System.Drawing.Size(131, 31);
            this.btnEditAffectation.TabIndex = 4;
            this.btnEditAffectation.Text = "Modifier l\'affectation";
            this.btnEditAffectation.UseVisualStyleBackColor = true;
            this.btnEditAffectation.Click += new System.EventHandler(this.btnEditAffectation_Click);
            // 
            // lblAffectationInfo
            // 
            this.lblAffectationInfo.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblAffectationInfo.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblAffectationInfo.Location = new System.Drawing.Point(3, 236);
            this.lblAffectationInfo.Name = "lblAffectationInfo";
            this.lblAffectationInfo.Size = new System.Drawing.Size(507, 37);
            this.lblAffectationInfo.TabIndex = 3;
            this.lblAffectationInfo.Text = "1 conducteur est affecté à votre ligne.";
            this.lblAffectationInfo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // frmManageLines
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(784, 461);
            this.Controls.Add(this.splitContainer1);
            this.MinimumSize = new System.Drawing.Size(800, 500);
            this.Name = "frmManageLines";
            this.ShowIcon = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Gestion des lignes";
            this.Load += new System.EventHandler(this.frmManageLines_Load);
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
            this.splitContainer1.ResumeLayout(false);
            this.tableLayoutPanel1.ResumeLayout(false);
            this.gbxLine.ResumeLayout(false);
            this.tableLayoutPanel2.ResumeLayout(false);
            this.tableLayoutPanel2.PerformLayout();
            this.tableLayoutPanel3.ResumeLayout(false);
            this.tableLayoutPanel3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.trbFrequency)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.SplitContainer splitContainer1;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.ListBox lbxLines;
        private System.Windows.Forms.Button btnNewLine;
        private System.Windows.Forms.GroupBox gbxLine;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel2;
        private System.Windows.Forms.Label lblLineName;
        private System.Windows.Forms.Button btnDelete;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel3;
        private System.Windows.Forms.TrackBar trbFrequency;
        private System.Windows.Forms.Label lblFrequency;
        private System.Windows.Forms.Button btnEditAffectation;
        private System.Windows.Forms.Label lblAffectationInfo;
    }
}